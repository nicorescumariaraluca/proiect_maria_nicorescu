package wantsome.project.ui.web.clients;

import io.javalin.http.Context;
import wantsome.project.db.dao.ClientDAO;
import wantsome.project.db.dto.Client;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * Handles the user interaction for adding or updating a client.
 */
public class AddEditClientsController {

    public static void showAddForm(Context ctx) {
        renderAddUpdateForm(ctx, -1, "", "", "", "", null); //empty form
    }

    public static void showUpdateForm(Context ctx) {

        //try to load the client from db (by id)
        long id = ctx.pathParamAsClass("id", Long.class).get();
        Optional<Client> optionalClient = ClientDAO.getById(id);
        Client client = optionalClient.orElseThrow(() -> new RuntimeException("Client with id " + id + " not found!"));

        //show the page with the form pre-filled with client details
        renderAddUpdateForm(ctx,
                client.getId(),
                client.getFirstName(),
                client.getLastName(),
                client.getEmail(),
                client.getAddress(),
                "");
    }

    /**
     * Render the add or update form. If id>=0, we are on 'update' case (else on 'add')
     */
    private static void renderAddUpdateForm(Context ctx, long id, String firstName, String lastName,
                                            String email, String address, String errorMsg) {
        Map<String, Object> model = new HashMap<>();

        //needed to be able to reuse same page for add and edit cases
        model.put("isUpdate", id >= 0);

        //default values for all visible inputs (relevant for update case)
        model.put("prevId", id);
        model.put("prevFirstName", firstName);
        model.put("prevLastName", lastName);
        model.put("prevEmail", email);
        model.put("prevAddress", address);

        //relevant for error cases (show validations errors)
        model.put("errorMsg", errorMsg);

        ctx.render("add_edit_client.vm", model);
    }

    public static void handleAddUpdatePostRequest(Context ctx) {
        //read form values (posted as params)
        long id = ctx.formParamAsClass("id", Long.class).getOrDefault(-1L); //id may be missing (for 'add' case), use -1 as default
        String firstName = ctx.formParam("firstName");
        String lastName = ctx.formParam("lastName");
        String email = ctx.formParam("email");
        String address = ctx.formParam("address");

        //handle separately the refresh case, if required (to just reload the page and recompute dynamic fields based on previous values from form)
        String serverAction = ctx.formParam("serverAction");
        if (Objects.equals(serverAction, "refresh")) {
            renderAddUpdateForm(ctx, id, firstName, lastName, email, address, null);
        } else {
            tryPerformAddUpdateAction(ctx, id, firstName, lastName, email, address);
        }
    }

    private static void tryPerformAddUpdateAction(Context ctx, long id, String firstName, String lastName,
                                                  String email, String address) {
        try {
            Client client = validateAndBuildClient(id, firstName, lastName, email, address);

            if (client.getId() > 0) { //if id is present (not -1), it's the 'update' case, else is 'add'
                ClientDAO.update(client);
            } else {
                ClientDAO.insert(client);
            }

            ctx.redirect("/clients"); //go back to parent page
        } catch (Exception e) {
            //remain on same page, reload form including also the error message
            renderAddUpdateForm(ctx, id, firstName, lastName, email, address, e.getMessage());
        }
    }

    /**
     * Method which validates input data (read as string values from form),
     * converts the values to right types, and builds a client from it
     */
    private static Client validateAndBuildClient(long id, String firstName, String lastName,
                                                 String email, String address) {

        if (firstName == null || firstName.isEmpty()) {
            throw new RuntimeException("First name is required!");
        }
        if (lastName == null || lastName.isEmpty()) {
            throw new RuntimeException("Last name is required!");
        }
        if (email == null || email.isEmpty()) {
            throw new RuntimeException("Email is required!");
        }
        if (address == null || address.isEmpty()) {
            throw new RuntimeException("Address is required!");
        }

        return new Client(id, firstName, lastName, email, address);
    }
}
