package wantsome.project.ui.web.clients;

import io.javalin.http.Context;
import wantsome.project.db.dao.ClientDAO;
import wantsome.project.db.dto.Client;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
 * Controller for Clients page - displaying the list of clients, with sorting/filtering options
 * <p>
 * Small page for add/edit a client has a separate controller: {@link AddEditClientsController}
 */

public class ClientsController {

    enum clientsOrder {
        FIRST_NAME, LAST_NAME
    }

    public static void showClientsPage(Context ctx) {
        renderClientsPage(ctx, null); //no error message the first time
    }

    private static void renderClientsPage(Context ctx, String errorMsg) {
        List<Client> allClients = ClientDAO.getAll();

        //get clients to display (after filtering and sorting)
        String filter = getFilterFromQueryOrSes(ctx);
        clientsOrder order = getOrderFromQueryOrSes(ctx);

        List<Client> clients = getClientsToDisplay(allClients, filter, order);

        Map<String, Object> model = new HashMap<>();
        model.put("clients", clients);
        model.put("crtFilter", filter);
        model.put("crtClientsOrder", order);
        model.put("errorMsg", errorMsg);

        ctx.render("clients.vm", model);
    }

    /**
     * Return the value to use for filtering by first Name, or null if no filter is applied
     */
    private static String getFilterFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "filter");
        if (param != null && param.isEmpty()) {
            return null;
        }
        return param;
    }

    private static clientsOrder getOrderFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "clientsOrder");
        return param == null ?
                clientsOrder.FIRST_NAME : //default
                clientsOrder.valueOf(param);
    }

    private static String fromQueryOrSession(Context ctx, String lastName) {
        String param = ctx.queryParam(lastName); //read it from current request (if sent)
        if (param != null) {
            ctx.sessionAttribute(lastName, param); //if found, save it to session for later
        } else {
            param = ctx.sessionAttribute(lastName); //if not found, try to read it from session
        }
        return param;
    }

    private static List<Client> getClientsToDisplay(List<Client> allClients, String nameFilter, clientsOrder order) {
        return allClients.stream()
                .filter(n -> nameFilter == null || n.getLastName() == nameFilter)
                .sorted(getDisplayComparator(order))
                .collect(toList());
    }

    private static Comparator<Client> getDisplayComparator(clientsOrder order) {
        if (order == clientsOrder.FIRST_NAME) {
            return Comparator.comparing(Client::getFirstName);
        } else {
            //by last name
            return (i1, i2) -> {
                String s1 = i1.getLastName();
                String s2 = i2.getLastName();
                return s1.compareTo(s2);
            };
        }
    }

    public static void handleDeleteRequest(Context ctx) {
        String errorMsg = null;
        try {
            long id = ctx.pathParamAsClass("id", Long.class).get();
            ClientDAO.delete(id);
        } catch (Exception e) {
            errorMsg = e.getMessage();
        }
        renderClientsPage(ctx, errorMsg); //clients page, possibly with error message (if error occurred)
    }

}
