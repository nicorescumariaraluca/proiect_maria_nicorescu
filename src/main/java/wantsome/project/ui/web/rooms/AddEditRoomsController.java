package wantsome.project.ui.web.rooms;

import io.javalin.http.Context;
import wantsome.project.db.dao.RoomDAO;
import wantsome.project.db.dao.RoomTypesDAO;
import wantsome.project.db.dto.Room;
import wantsome.project.db.dto.RoomTypes;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Handles the user interaction for adding or updating a room.
 */

public class AddEditRoomsController {

    public static void showAddForm(Context ctx) {
        renderAddForm(ctx, -1, -1, "", null); //empty form
    }

    public static void showUpdateForm(Context ctx) {

        long id = ctx.pathParamAsClass("id", Long.class).get();
        Optional<Room> optRoom = RoomDAO.getById(id);
        Room room = optRoom.orElseThrow(() -> new RuntimeException("Room with number " + id + " not found!"));

        //show the page with the form pre-filled with room details
        renderUpdateForm(ctx,
                room.getId(),
                room.getRoomTypeId(),
                room.getExtraInfo(),
                null);
    }

    /**
     * Render the add and update form
     */
    private static void renderAddForm(Context ctx, long id, long roomTypeId,
                                      String extraInfo, String errorMsg) {
        Map<String, Object> model = new HashMap<>();
        List<RoomTypes> allTypes = RoomTypesDAO.getAll();

        List<Long> idList = RoomDAO.getAll().stream()
                .map(Room::getId)
                .collect(Collectors.toList());

        //needed to be able to reuse same page for add and edit cases
        model.put("isIdUnique", !(idList.contains(id)));

        //default values for all visible inputs (relevant for update case)
        model.put("id", id);
        model.put("roomTypeId", roomTypeId);
        model.put("extraInfo", extraInfo);

        //relevant for error cases (show validations errors)
        model.put("errorMsg", errorMsg);

        //needed for populating the list of options in drop-down inputs
        model.put("allTypes", allTypes);

        ctx.render("add_room.vm", model);
    }

    private static void renderUpdateForm(Context ctx, long id, long roomTypeId,
                                         String extraInfo, String errorMsg) {
        Map<String, Object> model = new HashMap<>();
        List<RoomTypes> allTypes = RoomTypesDAO.getAll();

        List<Long> idList = RoomDAO.getAll().stream()
                .map(Room::getId)
                .collect(Collectors.toList());

        //needed to be able to reuse same page for add and edit cases
        model.put("isUpdate", idList.contains(id));

        //default values for all visible inputs (relevant for update case)
        model.put("prevId", id);
        model.put("prevRoomTypeId", roomTypeId);
        model.put("prevExtraInfo", extraInfo);

        //relevant for error cases (show validations errors)
        model.put("errorMsg", errorMsg);

        //needed for populating the list of options in drop-down inputs
        model.put("allTypes", allTypes);

        ctx.render("edit_room.vm", model);
    }

    public static void handleAddRequest(Context ctx) {
        //read form values (posted as params)
        long id = ctx.formParamAsClass("id", Long.class).get();
        long roomTypeId = ctx.formParamAsClass("roomTypeId", Long.class).get();
        String extraInfo = ctx.formParam("extraInfo");

        //handle separately the refresh case, if required (to just reload the page and recompute dynamic fields based on previous values from form)
        String serverAction = ctx.formParam("serverAction");
        if (Objects.equals(serverAction, "refresh")) {
            renderAddForm(ctx, id, roomTypeId, extraInfo, null);
        } else {
            tryPerformAddAction(ctx, id, roomTypeId, extraInfo);
        }
    }

    public static void handleUpdateRequest(Context ctx) {
        //read form values (posted as params)
        long id = ctx.formParamAsClass("id", Long.class).get();
        long roomTypeId = ctx.formParamAsClass("roomTypeId", Long.class).get();
        String extraInfo = ctx.formParam("extraInfo");

        //handle separately the refresh case, if required (to just reload the page and recompute dynamic fields based on previous values from form)
        String serverAction = ctx.formParam("serverAction");
        if (Objects.equals(serverAction, "refresh")) {
            renderUpdateForm(ctx, id, roomTypeId, extraInfo, null);
        } else {
            tryPerformUpdateAction(ctx, id, roomTypeId, extraInfo);
        }
    }

    private static void tryPerformAddAction(Context ctx, long id, long roomTypeId,
                                            String extraInfo) {
        try {
            Room room = validateAndBuildRoom(id, roomTypeId, extraInfo);
            List<Long> idList = RoomDAO.getAll().stream()
                    .map(Room::getId)
                    .collect(Collectors.toList());
            if (!(idList.contains(room.getId()))) {
                RoomDAO.insert(room);
            } else {
                throw new RuntimeException("Room number already assigned to another room! Pleas enter another room number");
            }

            ctx.redirect("/rooms"); //go back to parent page

        } catch (Exception e) {
            //remain on same page, reload form including also the error message
            renderAddForm(ctx, id, roomTypeId, extraInfo, e.getMessage());
        }
    }

    private static void tryPerformUpdateAction(Context ctx, long id, long roomTypeId,
                                               String extraInfo) {
        try {
            Room room = validateAndBuildRoom(id, roomTypeId, extraInfo);
            List<Long> idList = RoomDAO.getAll().stream()
                    .map(Room::getId)
                    .collect(Collectors.toList());

            if (idList.contains(room.getId())) {
                RoomDAO.update(room);
            }
            ctx.redirect("/rooms"); //go back to parent page

        } catch (Exception e) {
            //remain on same page, reload form including also the error message
            renderUpdateForm(ctx, id, roomTypeId, extraInfo, e.getMessage());
        }
    }

    /**
     * Method which validates input data (read as string values from form),
     * converts the values to right types, and builds a room from it
     */

    private static Room validateAndBuildRoom(long id, long roomTypeId, String extraInfo) {

        List<Long> idTypes = RoomTypesDAO.getAll().stream()
                .map(RoomTypes::getId)
                .collect(Collectors.toList());

        if (id <= 0) {
            throw new RuntimeException("Room number is required! Please enter a number");
        }
        if (roomTypeId <= 0 || !(idTypes.contains(roomTypeId))) {
            throw new RuntimeException("Room Type ID is required! Please select a type for room");
        }
        if (extraInfo == null || extraInfo.isEmpty()) {
            throw new RuntimeException("Room details (floor and view) are required!");
        }

        return new Room(id, roomTypeId, extraInfo);
    }

}
