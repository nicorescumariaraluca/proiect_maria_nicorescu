package wantsome.project.ui.web.rooms;

import io.javalin.http.Context;
import wantsome.project.db.dao.RoomDAO;
import wantsome.project.db.dto.RoomFull;

import java.util.*;

import static java.util.stream.Collectors.toList;

/**
 * Controller for Rooms page - displaying the list of rooms, with sorting/filtering options
 * Small page for add/edit a room has a separate controller: {@link AddEditRoomsController}
 */
public class RoomsController {

    public enum RoomsOrder {
        RATE, CAPACITY
    }

    public static void showRoomsPage(Context ctx) {
        renderRoomsPage(ctx, null); //no error message the first time
    }

    public static void renderRoomsPage(Context ctx, String errorMsg) {

        //get rooms to display (after sorting)
        RoomsOrder roomsOrder = getOrderFromQueryOrSes(ctx);

        String from = getFromFromQueryOrSes(ctx);
        String until = getUntilFromQueryOrSes(ctx);
        List<RoomFull> allRooms = RoomDAO.getFullAvailableFromSelection(from, until, null);

        String serverAction = ctx.formParam("serverAction");
        if (Objects.equals(serverAction, "refresh")) {
            from = getFromFromQueryOrSes(ctx);
            until = getUntilFromQueryOrSes(ctx);
            allRooms = RoomDAO.getFullAvailableFromSelection(from, until, null);
        }

        List<RoomFull> rooms = getRoomsToDisplay(allRooms, roomsOrder);

        long availableCount = allRooms.size();
        long reservedCount = RoomDAO.getAll().size() - availableCount;

        Map<String, Object> model = new HashMap<>();
        model.put("rooms", rooms);
        model.put("crtRoomsOrder", roomsOrder);
        model.put("from", from);
        model.put("until", until);
        model.put("availableCount", availableCount);
        model.put("reservedCount", reservedCount);
        model.put("errorMsg", errorMsg);

        ctx.render("rooms.vm", model);
    }

    /**
     * Return the value to use for filtering by type of room, or null if no filter is applied
     */

    private static RoomsOrder getOrderFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "roomsOrder");
        return param == null ?
                RoomsOrder.RATE : //default
                RoomsOrder.valueOf(param);
    }

    private static String getFromFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "from");
        if (param != null && param.isEmpty()) {
            return null;
        }
        return param;
    }

    private static String getUntilFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "until");
        if (param != null && param.isEmpty()) {
            return null;
        }
        return param;
    }

    private static String fromQueryOrSession(Context ctx, String name) {
        String param = ctx.queryParam(name); //read it from current request (if sent)
        if (param != null) {
            ctx.sessionAttribute(name, param); //if found, save it to session for later
        } else {
            param = ctx.sessionAttribute(name); //if not found, try to read it from session
        }
        return param;
    }

    private static List<RoomFull> getRoomsToDisplay(List<RoomFull> allRooms, RoomsOrder order) {
        return allRooms.stream()
                .sorted(getDisplayComparator(order))
                .collect(toList());
    }

    private static Comparator<RoomFull> getDisplayComparator(RoomsOrder order) {
        if (order == RoomsOrder.RATE) {
            return Comparator.comparing(RoomFull::getId);
        } else {
            return (i1, i2) -> {
                int r1 = i1.getCapacity();
                int r2 = i2.getCapacity();
                return r1 - r2;
            };
        }
    }

    public static void handleDeleteRequest(Context ctx) {
        String errorMsg = null;
        try {
            long id = ctx.pathParamAsClass("id", Long.class).get();
            RoomDAO.delete(id);
        } catch (Exception e) {
            errorMsg = e.getMessage();
        }
        renderRoomsPage(ctx, errorMsg);
    }

}
