package wantsome.project.ui.web.other;

import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.Context;
import wantsome.project.db.dao.RoomDAO;
import wantsome.project.db.dao.RoomTypesDAO;
import wantsome.project.db.dto.RoomTypes;
import wantsome.project.db.dto.Types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Controller for Home page - displaying the types of rooms, their capacity and rate, price policy
 */
public class HomeController {

    static {
        // need to register first some custom convertors, to convert string values read from form
        JavalinValidation.register(Types.class, s -> s != null && !s.isEmpty() ? Types.valueOf(s) : null);
    }

    public static void showHomePage(Context ctx) {
        renderHomePage(ctx, null); //no error message the first time
    }

    public static void renderHomePage(Context ctx, String errorMsg) {

        List<RoomTypes> roomTypesList = RoomTypesDAO.getAll();

        String serverAction = ctx.formParam("serverAction");
        if (Objects.equals(serverAction, "refresh")) {
            roomTypesList = RoomTypesDAO.getAll();
        }

        //data for graphs
        long singleCount = RoomDAO.getAvailableFromSelection(null, null, 1L).size();
        long doubleCount = RoomDAO.getAvailableFromSelection(null, null, 2L).size();
        long twinCount = RoomDAO.getAvailableFromSelection(null, null, 3L).size();
        long tripleCount = RoomDAO.getAvailableFromSelection(null, null, 4L).size();
        long suiteCount = RoomDAO.getAvailableFromSelection(null, null, 5L).size();


        Map<String, Object> model = new HashMap<>();
        model.put("roomTypesList", roomTypesList);
        model.put("singleCount", singleCount);
        model.put("doubleCount", doubleCount);
        model.put("twinCount", twinCount);
        model.put("tripleCount", tripleCount);
        model.put("suiteCount", suiteCount);
        model.put("totalCount", RoomDAO.getAll().size());
        model.put("errorMsg", errorMsg);

        ctx.render("main.vm", model);
    }
}
