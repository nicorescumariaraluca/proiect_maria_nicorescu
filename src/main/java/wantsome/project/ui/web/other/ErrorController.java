package wantsome.project.ui.web.other;

import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Controller for the generic error page displayed when an exception occurs
 */
public class ErrorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorController.class);

    public static void handleException(Exception exception, Context ctx) {
        // prepare a nice page as response for end user (with info and link to redirect to home page)
        Map<String, Object> model = new HashMap<>();
        model.put("errorMsg", exception.getMessage());
        ctx.render("error.vm", model);

        // but useful here to also write the exception details to logs, for later debug by developers
        LOGGER.error("An unexpected exception occurred: ", exception);
    }
}
