package wantsome.project.ui.web.other;

import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.Context;
import wantsome.project.db.dao.ReservationDAO;
import wantsome.project.db.dao.RoomDAO;
import wantsome.project.db.dao.RoomTypesDAO;
import wantsome.project.db.dto.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Controller for Reports page - displaying reports about:
 * -> hotel occupancy rate in period selected;
 * -> state of reservations in period selected;
 * -> structure of rooms by type.
 */
public class ReportsController {

    static {
        // need to register first some custom convertors, to convert string values read from form
        JavalinValidation.register(Types.class, s -> s != null && !s.isEmpty() ? Types.valueOf(s) : null);
    }

    public static void showReportsPage(Context ctx) {
        renderReportsPage(ctx, null); //no error message the first time
    }

    public static void renderReportsPage(Context ctx, String errorMsg) {

        String from = getFromFromQueryOrSes(ctx);
        String until = getUntilFromQueryOrSes(ctx);
        String start = getStartFromQueryOrSes(ctx);
        String end = getEndFromQueryOrSes(ctx);
        List<RoomFull> allRooms = RoomDAO.getFullAvailableFromSelection(from, until, null);
        List<ReservationFull> allReservations = ReservationDAO.getFullReservationsSelected(start, end);
        List<RoomTypes> roomTypesList = RoomTypesDAO.getAll();

        String serverAction = ctx.formParam("serverAction");

        if (Objects.equals(serverAction, "refresh")) {
            from = getFromFromQueryOrSes(ctx);
            until = getUntilFromQueryOrSes(ctx);
            allRooms = RoomDAO.getFullAvailableFromSelection(from, until, null);

            start = getStartFromQueryOrSes(ctx);
            end = getEndFromQueryOrSes(ctx);
            allReservations = ReservationDAO.getFullReservationsSelected(start, end);
        }

        //data for graphs
        long singleCount = RoomDAO.getAvailableFromSelection(null, null, 1L).size();
        long doubleCount = RoomDAO.getAvailableFromSelection(null, null, 2L).size();
        long twinCount = RoomDAO.getAvailableFromSelection(null, null, 3L).size();
        long tripleCount = RoomDAO.getAvailableFromSelection(null, null, 4L).size();
        long suiteCount = RoomDAO.getAvailableFromSelection(null, null, 5L).size();

        long availableCount = allRooms.size();
        long reservedCount = RoomDAO.getAll().size() - availableCount;
        long activeCount = allReservations.stream().filter(i -> Objects.equals(i.getState(), State.ACTIVE)).count();
        long paidCount = allReservations.stream().filter(i -> Objects.equals(i.getState(), State.PAID)).count();

        Map<String, Object> model = new HashMap<>();
        model.put("roomTypesList", roomTypesList);
        model.put("from", from);
        model.put("until", until);
        model.put("start", start);
        model.put("end", end);
        model.put("singleCount", singleCount);
        model.put("doubleCount", doubleCount);
        model.put("twinCount", twinCount);
        model.put("tripleCount", tripleCount);
        model.put("suiteCount", suiteCount);
        model.put("availableCount", availableCount);
        model.put("reservedCount", reservedCount);
        model.put("activeCount", activeCount);
        model.put("paidCount", paidCount);
        model.put("errorMsg", errorMsg);

        ctx.render("reports.vm", model);
    }

    private static String getFromFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "from");
        if (param != null && param.isEmpty()) {
            return null;
        }
        return param;
    }

    private static String getUntilFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "until");
        if (param != null && param.isEmpty()) {
            return null;
        }
        return param;
    }

    private static String getStartFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "start");
        if (param != null && param.isEmpty()) {
            return null;
        }
        return param;
    }

    private static String getEndFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "end");
        if (param != null && param.isEmpty()) {
            return null;
        }
        return param;
    }

    private static String fromQueryOrSession(Context ctx, String name) {
        String param = ctx.queryParam(name); //read it from current request (if sent)
        if (param != null) {
            ctx.sessionAttribute(name, param); //if found, save it to session for later
        } else {
            param = ctx.sessionAttribute(name); //if not found, try to read it from session
        }
        return param;
    }
}
