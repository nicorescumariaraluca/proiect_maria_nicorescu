package wantsome.project.ui.web.reservations;

import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.Context;
import org.apache.velocity.tools.generic.NumberTool;
import wantsome.project.db.dao.ClientDAO;
import wantsome.project.db.dao.ReservationDAO;
import wantsome.project.db.dao.RoomDAO;
import wantsome.project.db.dao.RoomTypesDAO;
import wantsome.project.db.dto.*;
import wantsome.project.db.service.Invoice;

import java.sql.Date;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;


/**
 * Handles the user interaction for adding or updating a reservation.
 */
public class AddEditReservationController {

    static {
        // need to register first some custom convertors, to convert string values read from form
        JavalinValidation.register(Date.class, s -> s != null && !s.isEmpty() ? Date.valueOf(s) : null);
        JavalinValidation.register(State.class, s -> s != null && !s.isEmpty() ? State.valueOf(s) : null);
        JavalinValidation.register(Types.class, s -> s != null && !s.isEmpty() ? Types.valueOf(s) : null);
    }

    /**
     * Handles the user interaction for adding a reservation for a specified client.
     */
    public static void showAddFormForClient(Context ctx) {
        long id = ctx.pathParamAsClass("id", Long.class).get();
        renderAddFormForClient(ctx, -1, id, new java.sql.Date(Calendar.getInstance().getTime().getTime()),
                new java.sql.Date(Calendar.getInstance().getTime().getTime()), 1L,
                "", State.ACTIVE, null);
        //empty form, only id of client is completed
    }

    public static void showUpdateForm(Context ctx) {

        //try to load the reservation from db (by id)
        long id = ctx.pathParamAsClass("id", Long.class).get();
        Optional<Reservation> optReservation = ReservationDAO.getById(id);
        Reservation reservation = optReservation.orElseThrow(() -> new RuntimeException("Reservation with id " + id + " not found!"));

        //show the page with the form pre-filled with reservation details
        renderUpdateForm(ctx,
                reservation.getId(),
                reservation.getClientId(),
                reservation.getStartDate(),
                reservation.getEndDate(),
                reservation.getRoomId(),
                reservation.getExtraInfo(),
                reservation.getState(),
                "");
    }

    /**
     * Render the add or update form. If id>=0, we are on 'update' case (else on 'add')
     */
    public static void renderUpdateForm(Context ctx, long id, long clientId, Date startDate, Date endDate,
                                        long roomId, String extraInfo, State state, String errorMsg) {

        Map<String, Object> model = new HashMap<>();
        List<RoomFull> roomsAvailable = RoomDAO.getFullAvailableFromSelection(startDate.toString(), endDate.toString(), null);
        Client client = ClientDAO.getById(clientId).orElse(null);
        assert client != null;
        String name = client.getFirstName() + " " + client.getLastName();
        Room room = RoomDAO.getById(roomId).orElse(null);
        assert room != null;
        RoomTypes roomTypes = RoomTypesDAO.getById(room.getRoomTypeId()).orElse(null);
        assert roomTypes != null;
        double standardRate = roomTypes.getRate();
        double totalPrice = Invoice.getTotalPrice(startDate, endDate, roomId, standardRate);

        //default values for all visible inputs (relevant for update case)
        model.put("prevId", id);
        model.put("prevClientId", clientId);
        model.put("name", name);
        model.put("prevStartDate", startDate);
        model.put("prevEndDate", endDate);
        model.put("prevRoomId", roomId);
        model.put("prevExtraInfo", extraInfo);
        model.put("prevState", state);
        model.put("numberTool", new NumberTool());

        //dynamic computed value, based on current due date (may change on refresh)
        model.put("daysUntilEndDate", computeDaysUntilEndDate(endDate));
        model.put("daysUntilCurrentDate", computeDaysUntilEndDate(startDate));
        model.put("daysBetween", Invoice.getDaysBetween(startDate, endDate));
        model.put("totalPrice", String.format("%,.2f", totalPrice));

        //relevant for error cases (show validations errors)
        model.put("errorMsg", errorMsg);

        //needed for populating the list of options in drop-down inputs
        model.put("roomsAvailable", roomsAvailable);
        model.put("stateOptions", State.values());
        model.put("statePaidOptions", List.of(State.PAID, State.CANCELED));

        ctx.render("edit_reservation.vm", model);
    }

    public static void renderAddFormForClient(Context ctx, long id, long clientId, Date startDate, Date endDate,
                                              long roomId, String extraInfo, State state, String errorMsg) {

        Map<String, Object> model = new HashMap<>();
        List<RoomFull> roomsAvailable = RoomDAO.getFullAvailableFromSelection(startDate.toString(), endDate.toString(), null);

        //Client data
        Client client = ClientDAO.getById(clientId).orElse(null);
        assert client != null;
        String firstName = client.getFirstName();
        String lastName = client.getLastName();
        String email = client.getEmail();
        String address = client.getAddress();

        //default values for all visible inputs (relevant for update case)
        model.put("prevId", id);
        model.put("prevClientId", clientId);
        model.put("prevStartDate", startDate);
        model.put("prevEndDate", endDate);
        model.put("prevRoomId", roomId);
        model.put("prevExtraInfo", extraInfo);
        model.put("prevState", state);
        model.put("firstName", firstName);
        model.put("lastName", lastName);
        model.put("email", email);
        model.put("address", address);

        //dynamic computed value, based on current due date (may change on refresh)
        model.put("daysUntilEndDate", computeDaysUntilEndDate(endDate));
        model.put("daysBetween", Invoice.getDaysBetween(startDate, endDate));

        //relevant for error cases (show validations errors)
        model.put("errorMsg", errorMsg);

        //needed for populating the list of options in drop-down inputs
        model.put("roomsAvailable", roomsAvailable);
        model.put("stateOptions", List.of(State.ACTIVE));

        ctx.render("add_reservation_client.vm", model);
    }

    private static long computeDaysUntilEndDate(Date date) {
        if (date != null) {
            try {
                LocalDateTime today = LocalDate.now().atStartOfDay();
                LocalDateTime endDate = date.toLocalDate().atStartOfDay();
                return Duration.between(today, endDate).toDays();
            } catch (Exception e) {
                System.err.println("Error computing days until end date '" + date + "': " + e.getMessage());
            }
        }
        return 0;
    }

    public static void handleUpdatePostRequest(Context ctx) {
        //read form values (posted as params)
        long id = ctx.formParamAsClass("id", Long.class).getOrDefault(-1L); //id may be missing (for 'add' case), use -1 as default
        long clientId = ctx.formParamAsClass("clientId", Long.class).get();
        Date startDate = ctx.formParamAsClass("startDate", Date.class).get();
        Date endDate = ctx.formParamAsClass("endDate", Date.class).get();
        long roomId = ctx.formParamAsClass("roomId", Long.class).get();
        String extraInfo = ctx.formParam("extraInfo");
        State state = ctx.formParamAsClass("state", State.class).get();

        //handle separately the refresh case, if required (to just reload the page and recompute dynamic fields based on previous values from form)
        String serverAction = ctx.formParam("serverAction");
        if (Objects.equals(serverAction, "refresh")) {
            renderUpdateForm(ctx, id, clientId, startDate, endDate, roomId, extraInfo, state, null);
        } else {
            tryPerformUpdateAction(ctx, id, clientId, startDate, endDate, roomId, extraInfo, state);
        }
    }

    public static void handleAddReservationForClientPostRequest(Context ctx) {
        //read form values (posted as params)
        long id = ctx.formParamAsClass("id", Long.class).getOrDefault(-1L); //id may be missing (for 'add' case), use -1 as default
        long clientId = ctx.formParamAsClass("clientId", Long.class).get();
        Date startDate = ctx.formParamAsClass("startDate", Date.class).get();
        Date endDate = ctx.formParamAsClass("endDate", Date.class).get();
        long roomId = ctx.formParamAsClass("roomId", Long.class).get();
        String extraInfo = ctx.formParam("extraInfo");
        State state = ctx.formParamAsClass("state", State.class).get();

        //handle separately the refresh case, if required (to just reload the page and recompute dynamic fields based on previous values from form)
        String serverAction = ctx.formParam("serverAction");
        if (Objects.equals(serverAction, "refresh")) {
            renderAddFormForClient(ctx, id, clientId, startDate, endDate, roomId, extraInfo, state, null);
        } else {
            tryPerformAddAction(ctx, id, clientId, startDate, endDate, roomId, extraInfo, state);
        }
    }

    private static void tryPerformAddAction(Context ctx, long id, long clientId, Date startDate,
                                            Date endDate, long roomId, String extraInfo, State state) {
        try {
            Reservation reservation = validateAndBuildReservation(id, clientId, startDate, endDate, roomId, extraInfo, state);

            if (reservation.getId() < 0 && computeDaysUntilEndDate(startDate) >= 0) {
                ReservationDAO.insert(reservation);
            }

            ctx.redirect("/reservations"); //go back to parent page
        } catch (Exception e) {
            //remain on same page, reload form including also the error message
            renderAddFormForClient(ctx, id, clientId, startDate, endDate, roomId, extraInfo, state, e.getMessage());
        }
    }

    private static void tryPerformUpdateAction(Context ctx, long id, long clientId, Date startDate,
                                               Date endDate, long roomId, String extraInfo, State state) {
        try {
            Reservation reservation = validateAndBuildReservation(id, clientId, startDate, endDate, roomId, extraInfo, state);

            if (reservation.getId() > 0) { //if id is present (not -1), it's the 'update' case, else is 'add'
                ReservationDAO.update(reservation);
            }

            ctx.redirect("/reservations"); //go back to parent page
        } catch (Exception e) {
            //remain on same page, reload form including also the error message
            renderUpdateForm(ctx, id, clientId, startDate, endDate, roomId, extraInfo, state, e.getMessage());
        }
    }

    /**
     * Method which validates input data (read as string values from form),
     * converts the values to right types, and builds a reservation from it
     */
    private static Reservation validateAndBuildReservation(long id, long clientId, Date startDate,
                                                           Date endDate, long roomId, String extraInfo, State state) {

        if (clientId <= 0) {
            throw new RuntimeException("Client ID is required!");
        }
        if (startDate == null) {
            throw new RuntimeException("Start date of reservation is required!");
        }
        if (endDate == null) {
            throw new RuntimeException("End date of reservation is required!");
        }
        if (roomId <= 0) {
            throw new RuntimeException("Room number is required!");
        }
        if (state == null) {
            throw new RuntimeException("State of reservation is required!");
        }

        return new Reservation(id, clientId, startDate, endDate, roomId, extraInfo, state);
    }
}
