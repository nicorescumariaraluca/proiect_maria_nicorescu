package wantsome.project.ui.web.reservations;

import io.javalin.http.Context;
import org.apache.velocity.tools.generic.DateTool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wantsome.project.db.dao.ClientDAO;
import wantsome.project.db.dao.ReservationDAO;
import wantsome.project.db.dto.Client;
import wantsome.project.db.dto.Reservation;
import wantsome.project.db.dto.ReservationFull;
import wantsome.project.db.dto.State;

import java.util.*;

import static java.util.stream.Collectors.*;

/**
 * Controller for Reservations page - displaying the list of reservations, with sorting/filtering options
 * <p>
 * Small page for add/edit reservation has a separate controller: {@link AddEditReservationController}
 * To compute invoice for a reservation has a separate controller: {@link InvoiceController}
 */

public class ReservationsController {
    private static final Logger logger = LoggerFactory.getLogger(ReservationsController.class);

    public enum reservationsOrder {
        START_DATE, END_DATE
    }

    public static void showReservationsPage(Context ctx) {

        //get reservations to display (after filtering and sorting)
        State filter = getFilterFromQueryOrSes(ctx);
        reservationsOrder order = getOrderFromQueryOrSes(ctx);

        String start = getStartFromQueryOrSes(ctx);
        String end = getEndFromQueryOrSes(ctx);
        List<ReservationFull> allReservations = ReservationDAO.getFullReservationsSelected(start, end);

        String serverAction = ctx.formParam("serverAction");

        if (Objects.equals(serverAction, "refresh")) {
            start = getStartFromQueryOrSes(ctx);
            end = getEndFromQueryOrSes(ctx);
            allReservations = ReservationDAO.getFullReservationsSelected(start, end);
        }

        List<ReservationFull> reservations = getFullReservationsToDisplay(allReservations, filter, order);

        //data for graphs
        long activeCount = allReservations.stream().filter(i -> Objects.equals(i.getState(), State.ACTIVE)).count();
        long paidCount = allReservations.stream().filter(i -> Objects.equals(i.getState(), State.PAID)).count();

        Map<String, Long> stateVsCountMap = allReservations.stream()
                .filter(n -> n.getState() == State.ACTIVE)
                .collect(groupingBy(n -> n.getEndDate().toString(), counting()));

        Map<String, Object> model = new HashMap<>();
        model.put("reservations", reservations);
        model.put("start", start);
        model.put("end", end);
        model.put("crtReservationsFilter", filter);
        model.put("crtReservationsOrder", order);
        model.put("activeCount", activeCount);
        model.put("paidCount", paidCount);
        model.put("stateVsCountMap", stateVsCountMap);
        model.put("dateTool", new DateTool());

        ctx.render("reservations.vm", model);
    }

    public static void showReservationsForClient(Context ctx) {

        long id = ctx.pathParamAsClass("id", Long.class).get();
        List<Reservation> allReservations = ReservationDAO.getReservationsByClient(id, null, null, null);

        //get reservations to display (after filtering and sorting)
        State filter = getFilterFromQueryOrSes(ctx);
        reservationsOrder order = getOrderFromQueryOrSes(ctx);

        List<Reservation> reservations = getReservationsToDisplay(allReservations, filter, order);

        //get Client details
        Client client = ClientDAO.getById(id).orElse(null);
        assert client != null;
        String firstName = client.getFirstName();
        String lastName = client.getLastName();
        long clientIdForTitle = client.getId();

        //data for graphs
        long activeCount = allReservations.stream().filter(i -> Objects.equals(i.getState(), State.ACTIVE)).count();
        long paidCount = allReservations.stream().filter(i -> Objects.equals(i.getState(), State.PAID)).count();

        Map<String, Long> stateVsCountMap = allReservations.stream()
                .filter(n -> n.getState() == State.ACTIVE)
                .collect(groupingBy(n -> n.getEndDate().toString(), counting()));

        Map<String, Object> model = new HashMap<>();
        model.put("reservations", reservations);
        model.put("crtReservationsFilter", filter);
        model.put("crtReservationsOrder", order);
        model.put("activeCount", activeCount);
        model.put("paidCount", paidCount);
        model.put("stateVsCountMap", stateVsCountMap);
        model.put("firstName", firstName);
        model.put("lastName", lastName);
        model.put("clientIdForTitle", clientIdForTitle);
        model.put("dateTool", new DateTool());
        model.put("id", id);

        ctx.render("client.vm", model);
    }

    /**
     * Return the value to use for filtering reservations by State in selected period
     */
    private static State getFilterFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "reservationsFilter");
        return param == null ?
                State.ACTIVE : //default value (first time)
                (param.equals("ALL") ?
                        null :
                        State.valueOf(param));
    }

    private static reservationsOrder getOrderFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "reservationsOrder");
        return param == null ?
                reservationsOrder.START_DATE : //default
                reservationsOrder.valueOf(param);
    }

    private static String getStartFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "start");
        if (param != null && param.isEmpty()) {
            return null;
        }
        return param;
    }

    private static String getEndFromQueryOrSes(Context ctx) {
        String param = fromQueryOrSession(ctx, "end");
        if (param != null && param.isEmpty()) {
            return null;
        }
        return param;
    }

    private static String fromQueryOrSession(Context ctx, String name) {
        String param = ctx.queryParam(name); //read it from current request (if sent)
        if (param != null) {
            ctx.sessionAttribute(name, param); //if found, save it to session for later
            logger.info("Found param not null");
        } else {
            param = ctx.sessionAttribute(name); //if not found, try to read it from session
            logger.info("found param null");
        }
        logger.info("Param has value " + param);
        return param;
    }

    private static List<Reservation> getReservationsToDisplay(List<Reservation> allReservations, State stateFilter, reservationsOrder order) {
        return allReservations.stream()
                .filter(r -> stateFilter == null || Objects.equals(r.getState(), stateFilter))
                .sorted(getDisplayComparator(order))
                .collect(toList());
    }

    private static List<ReservationFull> getFullReservationsToDisplay(List<ReservationFull> allReservations, State stateFilter, reservationsOrder order) {
        return allReservations.stream()
                .filter(r -> stateFilter == null || Objects.equals(r.getState(), stateFilter))
                .sorted(getFullDisplayComparator(order))
                .collect(toList());
    }

    private static Comparator<Reservation> getDisplayComparator(reservationsOrder order) {
        if (order == reservationsOrder.START_DATE) {
            return Comparator.comparing(Reservation::getStartDate);
        } else {
            return (i1, i2) -> {
                Date d1 = i1.getEndDate();
                Date d2 = i2.getEndDate();
                return d2.compareTo(d1);
            };
        }
    }

    private static Comparator<ReservationFull> getFullDisplayComparator(reservationsOrder order) {
        if (order == reservationsOrder.START_DATE) {
            return Comparator.comparing(ReservationFull::getStartDate);
        } else {
            return (i1, i2) -> {
                Date d1 = i1.getEndDate();
                Date d2 = i2.getEndDate();
                return d2.compareTo(d1);
            };
        }
    }

    public static void handleDeleteRequest(Context ctx) {
        long id = ctx.pathParamAsClass("id", Long.class).get();
        ReservationDAO.delete(id);
        ctx.redirect("/reservations");
    }
}
