package wantsome.project.ui.web.reservations;

import io.javalin.core.validation.JavalinValidation;
import io.javalin.http.Context;
import wantsome.project.db.dao.ClientDAO;
import wantsome.project.db.dao.ReservationDAO;
import wantsome.project.db.dao.RoomDAO;
import wantsome.project.db.dao.RoomTypesDAO;
import wantsome.project.db.dto.*;
import wantsome.project.db.service.Invoice;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Handles the user interaction when generating an invoice for a selected reservation.
 */
public class InvoiceController {

    static {
        // need to register first some custom convertors, to convert string values read from form
        JavalinValidation.register(Date.class, s -> s != null && !s.isEmpty() ? Date.valueOf(s) : null);
        JavalinValidation.register(State.class, s -> s != null && !s.isEmpty() ? State.valueOf(s) : null);
        JavalinValidation.register(Types.class, s -> s != null && !s.isEmpty() ? Types.valueOf(s) : null);
    }

    public static void showInvoice(Context ctx) {

        //try to load the reservation from db (by id)
        long id = ctx.pathParamAsClass("id", Long.class).get();
        Optional<Reservation> optReservation = ReservationDAO.getById(id);
        Reservation reservation = optReservation.orElseThrow(() -> new RuntimeException("Reservation with id " + id + " not found!"));

        //try to load client and room from db (by id) (additional information on the top of the invoice)
        Optional<Client> optClient = ClientDAO.getById(reservation.getClientId());
        Client client = optClient.orElseThrow(() -> new RuntimeException("Client for reservation with id " + id + " not found!"));
        String name = client.getFirstName() + " " + client.getLastName();

        Optional<Room> optRoom = RoomDAO.getById(reservation.getRoomId());
        Room room = optRoom.orElseThrow(() -> new RuntimeException("Room for reservation with id " + id + " not found!"));

        Optional<RoomTypes> optRoomTypes = RoomTypesDAO.getById(room.getRoomTypeId());
        RoomTypes roomType = optRoomTypes.orElseThrow(() -> new RuntimeException("Room Type for reservation with id " + id + " not found!"));

        //show the page with needed information to compute invoice for a reservation
        renderInvoiceRequest(ctx,
                reservation.getId(),
                reservation.getClientId(),
                reservation.getStartDate(),
                reservation.getEndDate(),
                reservation.getRoomId(),
                reservation.getExtraInfo(),
                reservation.getState(),
                name,
                roomType.getDescription(),
                roomType.getRate(),
                room.getExtraInfo(),
                "");
    }

    /**
     * Generate invoice for a reservation
     */
    public static void renderInvoiceRequest(Context ctx, long id, long clientId, Date startDate, Date endDate,
                                            long roomId, String extraInfo, State state, String name,
                                            String description, double rate, String extraInfoRoom, String errorMsg) {

        Map<String, Object> model = new HashMap<>();
        String invoice;
        invoice = Invoice.getInvoiceById(id);

        //default values for all visible fields needed to compute an invoice
        model.put("id", id);
        model.put("clientId", clientId);
        model.put("startDate", startDate);
        model.put("endDate", endDate);
        model.put("roomId", roomId);
        model.put("extraInfo", extraInfo);
        model.put("state", state);
        model.put("name", name);
        model.put("description", description);
        model.put("rate", String.format("%,.2f", rate));
        model.put("extraInfoRoom", extraInfoRoom);

        //dynamic computed value, based on current due date (may change on refresh)
        model.put("daysStandard", Invoice.getDaysBetween(startDate, endDate) - Invoice.getDaysSummerSeason(startDate, endDate) - Invoice.getDaysColdSeason(startDate, endDate));
        model.put("daysSummerSeason", Invoice.getDaysSummerSeason(startDate, endDate));
        model.put("daysColdSeason", Invoice.getDaysColdSeason(startDate, endDate));

        //relevant for error cases (show validations errors)
        model.put("errorMsg", errorMsg);

        model.put("invoice", invoice);

        ctx.render("invoice.vm", model);
    }
}
