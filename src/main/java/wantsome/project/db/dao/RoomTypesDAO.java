package wantsome.project.db.dao;

import wantsome.project.db.dto.RoomTypes;
import wantsome.project.db.service.DBManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Simple DAO which take cares of loading/persisting objects of type RoomTypes to DB
 */

public class RoomTypesDAO {

    /**
     * Load list of room types from DB (all found)
     */
    public static List<RoomTypes> getAll() {

        String sql = "SELECT * " +
                "FROM ROOM_TYPES " +
                "ORDER BY id ASC;";

        List<RoomTypes> roomTypes = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                roomTypes.add(extractRoomTypeFromResult(rs));
            }

        } catch (SQLException e) {
            System.err.println("Error loading all room types: " + e.getMessage());
        }

        return roomTypes;
    }

    /**
     * Load one specific roomType from DB (by id)
     */
    public static Optional<RoomTypes> getById(long id) {

        String sql = "SELECT * " +
                "FROM ROOM_TYPES " +
                "WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    RoomTypes roomType = extractRoomTypeFromResult(rs);
                    return Optional.of(roomType);
                }
            }

        } catch (SQLException e) {
            System.err.println("Error loading type with id: " + id + ": " + e.getMessage());
        }
        return Optional.empty(); //default value if not found
    }

    /**
     * Add new room type to DB
     */
    public static void insert(RoomTypes types) {

        String sql = "INSERT INTO ROOM_TYPES  " +
                "(id, description, rate, capacity)  " +
                "VALUES (?, ?, ?, ?);";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, types.getId());
            ps.setString(2, types.getDescription());
            ps.setDouble(3, types.getRate());
            ps.setInt(4, types.getCapacity());

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error inserting in DB room type " + types + ": " + e.getMessage());
        }
    }

    /**
     * Update an existing room type in DB
     */
    public static void update(RoomTypes types) {

        String sql = "UPDATE ROOM_TYPES  " +
                "SET description = ?, rate = ?, capacity = ?  " +
                "WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement (based on DTO data)
            ps.setString(1, types.getDescription());
            ps.setDouble(2, types.getRate());
            ps.setInt(3, types.getCapacity());
            ps.setLong(4, types.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Error while updating room type " + types + ": " + e.getMessage());
        }
    }

    /**
     * Delete a room from DB (by id)
     */
    public static void delete(long id) {

        String sql = "DELETE FROM ROOM_TYPES WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error while deleting room type with id " + id + ": " + e.getMessage());
        }
    }

    /**
     * Auxiliary method
     */
    private static RoomTypes extractRoomTypeFromResult(ResultSet rs) throws SQLException {

        long id = rs.getLong("id");
        String description = rs.getString("description");
        int rate = rs.getInt("rate");
        int capacity = rs.getInt("capacity");

        return new RoomTypes(id, description, rate, capacity);
    }
}
