package wantsome.project.db.dao;

import org.jetbrains.annotations.Nullable;
import wantsome.project.db.dto.Reservation;
import wantsome.project.db.dto.ReservationFull;
import wantsome.project.db.dto.State;
import wantsome.project.db.service.DBManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * DAO which take cares of loading/persisting objects of type Reservations to DB and from DB
 */

public class ReservationDAO {

    /**
     * Load list of reservations from DB (all found)
     */
    public static List<Reservation> getAll() {

        String sql = "SELECT * FROM RESERVATIONS ORDER BY start_date ASC, end_date ASC;";

        List<Reservation> reservations = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                reservations.add(extractReservationFromResult(rs));
            }

        } catch (SQLException e) {
            System.err.println("Error loading all reservations: " + e.getMessage());
        }
        return reservations;
    }

    /**
     * Load list of reservations from DB with full details (all found)
     */
    public static List<ReservationFull> getAllFull() {

        String sql = "SELECT RESERVATIONS.id , RESERVATIONS.client_id , CLIENTS.first_name , " +
                " CLIENTS.last_name , RESERVATIONS.start_date , RESERVATIONS.end_date , RESERVATIONS.room_id , " +
                " ROOM_TYPES.description , RESERVATIONS.extra_info , RESERVATIONS.status  " +
                "FROM RESERVATIONS " +
                "JOIN CLIENTS ON RESERVATIONS.client_id = CLIENTS.id  " +
                "JOIN ROOMS ON RESERVATIONS.room_id = ROOMS.id  " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id ;";

        List<ReservationFull> reservations = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                reservations.add(extractReservationFullFromResult(rs));
            }

        } catch (SQLException e) {
            System.err.println("Error loading all reservations: " + e.getMessage());
        }
        return reservations;
    }

    /**
     * Load reservation from DB by id
     */
    public static Optional<Reservation> getById(long id) {

        String sql = "SELECT * FROM RESERVATIONS WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    Reservation reservation = extractReservationFromResult(rs);
                    return Optional.of(reservation);
                }
            }

        } catch (SQLException e) {
            System.err.println("Error loading reservation with id: " + id + ": " + e.getMessage());
        }
        return Optional.empty(); //default value if not found
    }

    /**
     * Load list of reservations from DB from selected period (all of them)
     */
    public static List<Reservation> getReservationsSelected(String startDate, String endDate, State state, Long roomTypeId) {

        String sql = getSql(startDate, endDate, state, roomTypeId);

        List<Reservation> reservations = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            if (startDate != null && endDate != null) {
                ps.setString(1, startDate);
                ps.setString(2, endDate);

                if (state != null && roomTypeId == null) {
                    ps.setString(3, state.name());
                } else if (state == null && roomTypeId != null) {
                    ps.setLong(3, roomTypeId);
                } else if (state != null) {
                    ps.setString(3, state.name());
                    ps.setLong(4, roomTypeId);
                }

            } else if (startDate == null && endDate == null && state == null && roomTypeId != null) {
                ps.setLong(1, roomTypeId);
            }

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    reservations.add(extractReservationFromResult(rs));
                }
                return reservations;
            }

        } catch (SQLException e) {
            System.err.println("Error loading " + state + " reservations between: " + startDate
                    + " and " + endDate + " for rooms of type " + roomTypeId);
        }
        return reservations; //empty list if query is not executed
    }

    /**
     * Load list of reservations with full details from DB from selected period (all of them)
     */
    public static List<ReservationFull> getFullReservationsSelected(String startDate, String endDate) {

        String sql = getSqlFull(startDate, endDate);

        List<ReservationFull> reservations = new ArrayList<>();

        if (startDate == null && endDate == null) {
            reservations = getAllFull();
        }

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            if (startDate != null && endDate == null) {
                ps.setString(1, startDate);
            } else if (startDate == null && endDate != null) {
                ps.setString(1, endDate);
            } else if (startDate != null) {
                ps.setString(1, startDate);
                ps.setString(2, endDate);
            }

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    reservations.add(extractReservationFullFromResult(rs));
                }
                return reservations;
            }

        } catch (SQLException e) {
            System.err.println("Error loading reservations with full details between: " + startDate
                    + " and " + endDate + " for all rooms");
        }
        return reservations; //empty list if query is not executed
    }


    /**
     * Load all reservations from DB for specified client by first name and last name
     */
    public static List<Reservation> getReservationsByClient(Long clientId, String firstName, String lastName, State state) {

        String sql = getSqlByClient(clientId, firstName, lastName, state);

        List<Reservation> reservations = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            if (clientId != null && firstName == null && lastName == null && state == null) {
                ps.setLong(1, clientId);
            } else if (clientId == null && firstName != null && lastName != null) {
                if (state == null) {
                    ps.setString(1, firstName);
                    ps.setString(2, lastName);
                } else {
                    ps.setString(1, firstName);
                    ps.setString(2, lastName);
                    ps.setString(3, state.name());
                }
            }

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    reservations.add(extractReservationFromResult(rs));
                }
                return reservations;
            }

        } catch (SQLException e) {
            System.err.println("Error loading " + state + " reservation for client: " + clientId + " " + firstName
                    + " " + lastName);
        }
        return reservations; //empty list if query is not executed
    }

    /**
     * Add new reservation to DB
     */
    public static void insert(Reservation reservation) {

        String sql = "INSERT INTO RESERVATIONS (client_id, start_date, end_date, room_id, extra_info, status)" +
                " VALUES (?,?,?,?,?,?);";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, reservation.getClientId());
            ps.setString(2, reservation.getStartDate().toString());
            ps.setString(3, reservation.getEndDate().toString());
            ps.setLong(4, reservation.getRoomId());
            ps.setString(5, reservation.getExtraInfo());
            ps.setString(6, reservation.getState().name());

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error inserting in DB reservation " + reservation + ": " + e.getMessage());
        }
    }

    /**
     * Update an existing reservation in DB
     */
    public static void update(Reservation reservation) {

        String sql = "UPDATE RESERVATIONS " +
                "SET start_date = ?, end_date = ?, room_id = ?, extra_info = ?, status = ?  " +
                "WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement (based on DTO data)
            ps.setString(1, reservation.getStartDate().toString());
            ps.setString(2, reservation.getEndDate().toString());
            ps.setLong(3, reservation.getRoomId());
            ps.setString(4, reservation.getExtraInfo());
            ps.setString(5, reservation.getState().name());
            ps.setLong(6, reservation.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Error updating in DB reservation " + reservation + ": " + e.getMessage(), e);
        }
    }

    /**
     * Delete a reservation from DB (by id)
     */
    public static void delete(long id) {

        String sql = "DELETE FROM RESERVATIONS WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            throw new RuntimeException("Error while deleting reservation with id " + id + ": " + e.getMessage(), e);
        }
    }

    /**
     * Auxiliary methods
     */
    private static Reservation extractReservationFromResult(ResultSet rs) throws SQLException {

        long id = rs.getLong("id");
        long clientId = rs.getLong("client_id");
        Date startDate = rs.getDate("start_date");
        Date endDate = rs.getDate("end_date");
        long roomId = rs.getLong("room_id");
        String extraInfo = rs.getString("extra_info");
        State state = State.valueOf(rs.getString("status"));

        return new Reservation(id, clientId, startDate, endDate, roomId, extraInfo, state);
    }

    private static ReservationFull extractReservationFullFromResult(ResultSet rs) throws SQLException {

        long id = rs.getLong("id");
        long clientId = rs.getLong("client_id");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        Date startDate = rs.getDate("start_date");
        Date endDate = rs.getDate("end_date");
        long roomId = rs.getLong("room_id");
        String description = rs.getString("description");
        String extraInfo = rs.getString("extra_info");
        State state = State.valueOf(rs.getString("status"));

        return new ReservationFull(id, clientId, firstName, lastName, startDate,
                endDate, roomId, description, extraInfo, state);
    }

    @Nullable
    private static String getSql(String startDate, String endDate, State state, Long roomTypeId) {

        String sqlAllBetween = "SELECT * " +
                "FROM RESERVATIONS " +
                "WHERE start_date >= STRFTIME(?) " +
                " AND end_date <= STRFTIME(?);";

        String sqlStateBetween = "SELECT * " +
                "FROM RESERVATIONS " +
                "WHERE start_date >= STRFTIME(?) " +
                " AND end_date <= STRFTIME(?) " +
                " AND status LIKE ?;";

        String sqlAllByType = "SELECT *  " +
                "FROM RESERVATIONS " +
                "JOIN ROOMS ON RESERVATIONS.room_id = ROOMS.id  " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id  " +
                "WHERE ROOM_TYPES.id = ?;";

        String sqlBetweenByType = "SELECT RESERVATIONS.id , RESERVATIONS.client_id , RESERVATIONS.start_date , RESERVATIONS.end_date ,\n" +
                "RESERVATIONS.room_id , RESERVATIONS.extra_info , RESERVATIONS.status  " +
                "FROM RESERVATIONS " +
                "JOIN ROOMS ON RESERVATIONS.room_id = ROOMS.id  " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id  " +
                "WHERE start_date >= STRFTIME(?) " +
                " AND end_date <= STRFTIME(?) " +
                " AND ROOM_TYPES.id = ?;";

        String sqlBetweenByTypeAndState = "SELECT RESERVATIONS.id , RESERVATIONS.client_id , RESERVATIONS.start_date , RESERVATIONS.end_date ,\n" +
                "RESERVATIONS.room_id , RESERVATIONS.extra_info , RESERVATIONS.status  " +
                "FROM RESERVATIONS " +
                "JOIN ROOMS ON RESERVATIONS.room_id = ROOMS.id " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                "WHERE start_date >= STRFTIME(?) " +
                " AND end_date <= STRFTIME(?) " +
                " AND RESERVATIONS.status LIKE ? " +
                " AND ROOM_TYPES.id = ?;";


        String sql = null;

        if (startDate != null && endDate != null) {
            if (state == null && roomTypeId == null) {
                sql = sqlAllBetween;
            } else if (state != null && roomTypeId == null) {
                sql = sqlStateBetween;
            } else if (state == null) {
                sql = sqlBetweenByType;
            } else {
                sql = sqlBetweenByTypeAndState;
            }
        } else if (startDate == null && endDate == null && state == null && roomTypeId != null) {
            sql = sqlAllByType;
        }
        return sql;
    }

    private static String getSqlFull(String startDate, String endDate) {

        String sql = "SELECT RESERVATIONS.id , RESERVATIONS.client_id , CLIENTS.first_name , " +
                " CLIENTS.last_name , RESERVATIONS.start_date , RESERVATIONS.end_date , RESERVATIONS.room_id , " +
                " ROOM_TYPES.description , RESERVATIONS.extra_info , RESERVATIONS.status  " +
                "FROM RESERVATIONS " +
                "JOIN CLIENTS ON RESERVATIONS.client_id = CLIENTS.id  " +
                "JOIN ROOMS ON RESERVATIONS.room_id = ROOMS.id  " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id ";

        String from = "WHERE RESERVATIONS.start_date >= STRFTIME(?);";

        String until = "WHERE RESERVATIONS.end_date <= STRFTIME(?);";

        String between = "WHERE RESERVATIONS.start_date >= STRFTIME(?) " +
                " AND RESERVATIONS.end_date <= STRFTIME(?);";


        String sqlFull = "";

        if (startDate != null && endDate == null) {
            sqlFull = sql + from;
        } else if (startDate == null && endDate != null) {
            sqlFull = sql + until;
        } else if (startDate != null) {
            sqlFull = sql + between;
        }

        return sqlFull;
    }

    @Nullable
    private static String getSqlByClient(Long clientId, String firstName, String lastName, State state) {

        String sqlByClientId = "SELECT * " +
                "FROM RESERVATIONS " +
                "WHERE client_id = ?;";

        String sqlByClientName = "SELECT RESERVATIONS.id , RESERVATIONS.client_id , RESERVATIONS.start_date , RESERVATIONS.end_date ,\n" +
                "RESERVATIONS.room_id , RESERVATIONS.extra_info , RESERVATIONS.status   " +
                "FROM RESERVATIONS " +
                "JOIN CLIENTS ON RESERVATIONS.client_id = CLIENTS.id  " +
                "WHERE LOWER(CLIENTS.first_name) LIKE LOWER(?) " +
                " AND LOWER(CLIENTS.last_name) LIKE LOWER(?);";

        String sqlByClientNameAndState = "SELECT RESERVATIONS.id , RESERVATIONS.client_id , RESERVATIONS.start_date , RESERVATIONS.end_date ,\n" +
                "RESERVATIONS.room_id , RESERVATIONS.extra_info , RESERVATIONS.status  " +
                "FROM RESERVATIONS " +
                "JOIN CLIENTS ON RESERVATIONS.client_id = CLIENTS.id " +
                "WHERE LOWER(CLIENTS.first_name) LIKE LOWER(?) " +
                " AND LOWER(CLIENTS.last_name) LIKE LOWER(?) " +
                " AND RESERVATIONS.end_date >= DATE() " +
                " AND RESERVATIONS.status LIKE ?;";

        String sql = null;

        if (clientId != null && firstName == null && lastName == null && state == null) {
            sql = sqlByClientId;
        } else if (clientId == null && firstName != null && lastName != null) {
            if (state == null) {
                sql = sqlByClientName;
            } else {
                sql = sqlByClientNameAndState;
            }
        }
        return sql;
    }
}
