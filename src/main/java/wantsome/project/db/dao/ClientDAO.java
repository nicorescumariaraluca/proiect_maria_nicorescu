package wantsome.project.db.dao;

import org.jetbrains.annotations.Nullable;
import wantsome.project.db.dto.Client;
import wantsome.project.db.dto.State;
import wantsome.project.db.service.DBManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * Simple DAO which take cares of loading/persisting objects of type Client to DB
 */

public class ClientDAO {


    /**
     * Load list of clients from DB (all found)
     */
    public static List<Client> getAll() {

        String sql = "SELECT * " +
                "FROM CLIENTS " +
                "ORDER BY first_name ASC, last_name ASC;";

        List<Client> clients = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                clients.add(extractClientFromResult(rs));
            }

        } catch (SQLException e) {
            System.err.println("Error loading all clients: " + e.getMessage());
        }

        return clients;
    }

    /**
     * Load one specific client from DB (by id)
     */
    public static Optional<Client> getById(long id) {

        String sql = "SELECT * " +
                "FROM CLIENTS " +
                "WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    Client client = extractClientFromResult(rs);
                    return Optional.of(client);
                }
            }

        } catch (SQLException e) {
            System.err.println("Error loading client with id: " + id + ": " + e.getMessage());
        }
        return Optional.empty(); //default value if not found
    }

    /**
     * Load all clients (list) from DB by first name and last name and state of reservation (if selected)
     */
    public static List<Client> getAllBySelection(String firstName, String lastName, State state) {

        String sql = getSql(firstName, lastName, state);

        List<Client> clients = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            if (firstName != null && lastName != null) {
                if (state == null) {
                    ps.setString(1, firstName);
                    ps.setString(2, lastName);
                } else {
                    ps.setString(1, firstName);
                    ps.setString(2, lastName);
                    ps.setString(3, state.name());
                }
            } else if (firstName != null) {
                if (state == null) {
                    ps.setString(1, firstName);
                } else {
                    ps.setString(1, firstName);
                    ps.setString(2, state.name());
                }
            } else if (lastName != null) {
                if (state == null) {
                    ps.setString(1, lastName);
                } else {
                    ps.setString(1, lastName);
                    ps.setString(2, state.name());
                }
            }

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    clients.add(extractClientFromResult(rs));
                }
                return clients;
            }

        } catch (SQLException e) {
            System.err.println("Error loading clients with name: " + firstName + " " + lastName
                    + " and reservations of state " + state);
        }
        return clients; //empty list if query is not executed
    }

    /**
     * Add new client to DB
     */
    public static void insert(Client client) {

        String sql = "INSERT INTO CLIENTS  " +
                "(first_name, last_name, email, address)  " +
                "VALUES (?,?,?,?);";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setString(1, client.getFirstName());
            ps.setString(2, client.getLastName());
            ps.setString(3, client.getEmail());
            ps.setString(4, client.getAddress());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed") ?
                    "The email address you added already exists and its related to another client" :
                    e.getMessage();
            throw new RuntimeException("Error while inserting client: " + details);
        }
    }

    /**
     * Update an existing client in DB
     */
    public static void update(Client client) {

        String sql = "UPDATE CLIENTS  " +
                "SET first_name = ?, last_name = ?, email = ?, address = ?  " +
                "WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement (based on DTO data)
            ps.setString(1, client.getFirstName());
            ps.setString(2, client.getLastName());
            ps.setString(3, client.getEmail());
            ps.setString(4, client.getAddress());
            ps.setLong(5, client.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            String details = e.getMessage().contains("UNIQUE constraint failed") ?
                    "The email address you added already exists and its related to another client" :
                    e.getMessage();
            throw new RuntimeException("Error while updating client: " + details);
        }
    }

    /**
     * Delete a client from DB (by id)
     */
    public static void delete(long id) {

        String sql = "DELETE FROM CLIENTS WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("FOREIGN KEY constraint failed") ?
                    "Client has some existing reservations" :
                    e.getMessage();
            throw new RuntimeException("Error deleting client with id " + id + ": " + details);
        }
    }

    /**
     * Auxiliary methods
     */
    private static Client extractClientFromResult(ResultSet rs) throws SQLException {

        long id = rs.getLong("id");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        String email = rs.getString("email");
        String address = rs.getString("address");

        return new Client(id, firstName, lastName, email, address);
    }

    @Nullable
    private static String getSql(String firstName, String lastName, State state) {
        String sqlByFirstName = "SELECT * " +
                "FROM CLIENTS " +
                "WHERE LOWER(CLIENTS.first_name) LIKE LOWER(?);";

        String sqlByFirstNameAndState = "SELECT CLIENTS.id, CLIENTS.first_name, CLIENTS.last_name, CLIENTS.email, CLIENTS.address  " +
                "FROM CLIENTS " +
                "JOIN RESERVATIONS ON CLIENTS.id = RESERVATIONS.client_id  " +
                "WHERE LOWER(CLIENTS.first_name) LIKE LOWER(?) " +
                "AND RESERVATIONS.status LIKE ?;";

        String sqlByLastName = "SELECT *  " +
                "FROM CLIENTS " +
                "WHERE LOWER(CLIENTS.last_name) LIKE LOWER(?);";

        String sqlByLastNameAndState = "SELECT CLIENTS.id, CLIENTS.first_name, CLIENTS.last_name, CLIENTS.email, CLIENTS.address  " +
                "FROM CLIENTS " +
                "JOIN RESERVATIONS ON CLIENTS.id = RESERVATIONS.client_id  " +
                "WHERE LOWER(CLIENTS.last_name) LIKE LOWER(?) " +
                "AND RESERVATIONS.status LIKE ?;";

        String sqlByName = "SELECT * " +
                "FROM CLIENTS " +
                "WHERE LOWER(CLIENTS.first_name) LIKE LOWER(?) " +
                " AND LOWER(CLIENTS.last_name) LIKE LOWER(?);";

        String sqlByNameAndState = "SELECT CLIENTS.id, CLIENTS.first_name, CLIENTS.last_name, CLIENTS.email, CLIENTS.address  " +
                "FROM CLIENTS " +
                "JOIN RESERVATIONS ON CLIENTS.id = RESERVATIONS.client_id  " +
                "WHERE LOWER(CLIENTS.first_name) LIKE LOWER(?) " +
                "AND LOWER(CLIENTS.last_name) LIKE LOWER(?) " +
                "AND RESERVATIONS.status LIKE ?;";

        String sql = null;

        if (firstName != null && lastName != null) {
            if (state == null) {
                sql = sqlByName;
            } else {
                sql = sqlByNameAndState;
            }
        } else if (firstName != null) {
            if (state == null) {
                sql = sqlByFirstName;
            } else {
                sql = sqlByFirstNameAndState;
            }
        } else if (lastName != null) {
            if (state == null) {
                sql = sqlByLastName;
            } else {
                sql = sqlByLastNameAndState;
            }
        }
        return sql;
    }
}
