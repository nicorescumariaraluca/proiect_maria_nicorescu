package wantsome.project.db.dao;

import org.jetbrains.annotations.Nullable;
import wantsome.project.db.dto.Room;
import wantsome.project.db.dto.RoomFull;
import wantsome.project.db.service.DBManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Simple DAO which take cares of loading/persisting objects of type Room to DB
 */

public class RoomDAO {

    /**
     * Load list of rooms from DB (all found)
     */
    public static List<Room> getAll() {

        String sql = "SELECT * " +
                "FROM ROOMS " +
                "ORDER BY id ASC, room_type_id ASC;";

        List<Room> rooms = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                rooms.add(extractRoomFromResult(rs));
            }

        } catch (SQLException e) {
            System.err.println("Error loading all rooms: " + e.getMessage());
        }

        return rooms;
    }

    /**
     * Load list of rooms with full details from DB (all found)
     */
    public static List<RoomFull> getAllFull() {

        String sql = "SELECT ROOMS.id , ROOMS.room_type_id , ROOM_TYPES.description , " +
                " ROOM_TYPES.capacity , ROOM_TYPES.rate , ROOMS.extra_info " +
                "FROM ROOMS " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id;";

        List<RoomFull> rooms = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {

            while (rs.next()) {
                rooms.add(extractRoomFullFromResult(rs));
            }

        } catch (SQLException e) {
            System.err.println("Error loading all rooms with full details: " + e.getMessage());
        }

        return rooms;
    }

    /**
     * Load one specific room from DB (by id)
     */
    public static Optional<Room> getById(long id) {

        String sql = "SELECT *  " +
                "FROM ROOMS " +
                "WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    Room room = extractRoomFromResult(rs);
                    return Optional.of(room);
                }
            }

        } catch (SQLException e) {
            System.err.println("Error loading room with id: " + id + ": " + e.getMessage());
        }
        return Optional.empty(); //default value if not found
    }

    /**
     * Load all available rooms from DB from selected period of specified type (if specified)
     */
    public static List<Room> getAvailableFromSelection(String startDate, String endDate, Long roomTypeId) {

        String sql = getSql(startDate, endDate, roomTypeId);

        List<Room> rooms = new ArrayList<>();

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            if (startDate == null && endDate == null && roomTypeId != null) {
                ps.setLong(1, roomTypeId);
            } else if (startDate != null && endDate != null) {
                if (roomTypeId == null) {
                    ps.setString(1, startDate);
                    ps.setString(2, endDate);
                } else {
                    ps.setLong(1, roomTypeId);
                    ps.setString(2, startDate);
                    ps.setString(3, endDate);
                    ps.setLong(4, roomTypeId);
                }
            }

            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    rooms.add(extractRoomFromResult(rs));
                }
                return rooms;
            }

        } catch (SQLException e) {
            System.err.println("Error loading available rooms of type " + roomTypeId + " between: " + startDate + " and " + endDate);
        }
        return rooms; //empty list if query is not executed
    }

    /**
     * Load all available rooms with full details from DB from selected period of specified type (if specified)
     */
    public static List<RoomFull> getFullAvailableFromSelection(String startDate, String endDate, Long roomTypeId) {

        List<RoomFull> rooms = new ArrayList<>();

        if (startDate == null && endDate == null && roomTypeId == null) {
            rooms = getAllFull();
        } else {
            String sql = getSql(startDate, endDate, roomTypeId);
            try (Connection conn = DBManager.getConnection();
                 PreparedStatement ps = conn.prepareStatement(sql)) {

                if (startDate == null && endDate == null) {
                    ps.setLong(1, roomTypeId);
                } else if (startDate != null && endDate != null) {
                    if (roomTypeId == null) {
                        ps.setString(1, startDate);
                        ps.setString(2, endDate);
                    } else {
                        ps.setLong(1, roomTypeId);
                        ps.setString(2, startDate);
                        ps.setString(3, endDate);
                        ps.setLong(4, roomTypeId);
                    }
                } else if (startDate != null) {
                    if (roomTypeId == null) {
                        ps.setString(1, startDate);
                    } else {
                        ps.setLong(1, roomTypeId);
                        ps.setString(2, startDate);
                        ps.setLong(3, roomTypeId);
                    }
                } else {
                    if (roomTypeId == null) {
                        ps.setString(1, endDate);
                    } else {
                        ps.setLong(1, roomTypeId);
                        ps.setString(2, endDate);
                        ps.setLong(3, roomTypeId);
                    }
                }

                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        rooms.add(extractRoomFullFromResult(rs));
                    }
                    return rooms;
                }

            } catch (SQLException e) {
                System.err.println("Error loading available rooms with full details of type " + roomTypeId + " between: " + startDate + " and " + endDate);
            }
        }
        return rooms; //empty list if query is not executed
    }

    /**
     * Add new room to DB
     */
    public static void insert(Room room) {

        String sql = "INSERT INTO ROOMS  " +
                "(id, room_type_id, extra_info)  " +
                "VALUES (?, ?, ?);";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, room.getId());
            ps.setLong(2, room.getRoomTypeId());
            ps.setString(3, room.getExtraInfo());

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("A PRIMARY KEY constraint failed") ?
                    "You entered a room number that already exists and its related to another room" :
                    e.getMessage();
            throw new RuntimeException("Error inserting room: " + details);
        }
    }

    /**
     * Update an existing room in DB
     */
    public static void update(Room room) {

        String sql = "UPDATE ROOMS  " +
                "SET room_type_id = ?, extra_info = ?  " +
                "WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            //set params for the prepared statement (based on DTO data)
            ps.setLong(1, room.getRoomTypeId());
            ps.setString(2, room.getExtraInfo());
            ps.setLong(3, room.getId());

            ps.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException("Error while updating room " + room + ": " + e.getMessage());
        }
    }

    /**
     * Delete a room from DB (by id)
     */
    public static void delete(long id) {

        String sql = "DELETE FROM ROOMS WHERE id = ?;";

        try (Connection conn = DBManager.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql)) {

            ps.setLong(1, id);

            ps.execute();

        } catch (SQLException e) {
            String details = e.getMessage().contains("FOREIGN KEY constraint failed") ?
                    "Room is used by existing reservations" :
                    e.getMessage();
            throw new RuntimeException("Error deleting room with number " + id + ": " + details);
        }
    }

    /**
     * Auxiliary methods
     */
    private static Room extractRoomFromResult(ResultSet rs) throws SQLException {

        long id = rs.getLong("id");
        long roomTypeId = rs.getLong("room_type_id");
        String extraInfo = rs.getString("extra_info");

        return new Room(id, roomTypeId, extraInfo);
    }

    private static RoomFull extractRoomFullFromResult(ResultSet rs) throws SQLException {

        long id = rs.getLong("id");
        long roomTypeId = rs.getLong("room_type_id");
        String description = rs.getString("description");
        int capacity = rs.getInt("capacity");
        int rate = rs.getInt("rate");
        String extraInfo = rs.getString("extra_info");

        return new RoomFull(id, roomTypeId, description, capacity, rate, extraInfo);
    }

    @Nullable
    private static String getSql(String startDate, String endDate, Long roomTypeId) {
        String sqlAvailableBetweenByType = "SELECT ROOMS.id , ROOMS.room_type_id , ROOM_TYPES.description , " +
                " ROOM_TYPES.capacity , ROOM_TYPES.rate , ROOMS.extra_info " +
                "FROM ROOMS " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                "WHERE ROOM_TYPES.id = ? " +
                " AND ROOMS.id NOT IN (SELECT ROOMS.id " +
                "  FROM ROOMS " +
                "  JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                "  JOIN RESERVATIONS ON ROOMS.id = RESERVATIONS.room_id " +
                "  WHERE start_date >= STRFTIME(?) " +
                "  AND end_date <= STRFTIME(?) " +
                "  AND ROOM_TYPES.id = ? " +
                "  AND RESERVATIONS.status NOT LIKE 'CANCELED');";

        String sqlAvailableBetween = "SELECT ROOMS.id , ROOMS.room_type_id , ROOM_TYPES.description , " +
                " ROOM_TYPES.capacity , ROOM_TYPES.rate , ROOMS.extra_info " +
                "FROM ROOMS " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                "WHERE ROOMS.id NOT IN (SELECT ROOMS.id " +
                " FROM ROOMS " +
                " JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                " JOIN RESERVATIONS ON ROOMS.id = RESERVATIONS.room_id " +
                " WHERE start_date >= STRFTIME(?) " +
                " AND end_date <= STRFTIME(?) " +
                " AND RESERVATIONS.status NOT LIKE 'CANCELED');";

        String sqlAllByRoomTypeId = "SELECT * " +
                "FROM ROOMS " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                "WHERE ROOMS.room_type_id = ?;";

        String sqlAvailableFromNoType = "SELECT ROOMS.id , ROOMS.room_type_id , ROOM_TYPES.description , " +
                " ROOM_TYPES.capacity , ROOM_TYPES.rate , ROOMS.extra_info " +
                "FROM ROOMS " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                "WHERE ROOMS.id NOT IN (SELECT ROOMS.id " +
                " FROM ROOMS " +
                " JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                " JOIN RESERVATIONS ON ROOMS.id = RESERVATIONS.room_id " +
                " WHERE start_date >= STRFTIME(?) " +
                " AND RESERVATIONS.status NOT LIKE 'CANCELED');";

        String sqlAvailableUntilNoType = "SELECT ROOMS.id , ROOMS.room_type_id , ROOM_TYPES.description , " +
                " ROOM_TYPES.capacity , ROOM_TYPES.rate , ROOMS.extra_info " +
                "FROM ROOMS " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                "WHERE ROOMS.id NOT IN (SELECT ROOMS.id " +
                " FROM ROOMS " +
                " JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                " JOIN RESERVATIONS ON ROOMS.id = RESERVATIONS.room_id " +
                " WHERE end_date <= STRFTIME(?) " +
                " AND RESERVATIONS.status NOT LIKE 'CANCELED');";

        String sqlAvailableFromByType = "SELECT ROOMS.id , ROOMS.room_type_id , ROOM_TYPES.description , " +
                " ROOM_TYPES.capacity , ROOM_TYPES.rate , ROOMS.extra_info " +
                "FROM ROOMS " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                "WHERE ROOM_TYPES.id = ? " +
                " AND ROOMS.id NOT IN (SELECT ROOMS.id " +
                " FROM ROOMS " +
                " JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                " JOIN RESERVATIONS ON ROOMS.id = RESERVATIONS.room_id " +
                " WHERE start_date >= STRFTIME(?) " +
                " AND ROOM_TYPES.id = ? " +
                " AND RESERVATIONS.status NOT LIKE 'CANCELED');";

        String sqlAvailableUntilByType = "SELECT ROOMS.id , ROOMS.room_type_id , ROOM_TYPES.description , " +
                " ROOM_TYPES.capacity , ROOM_TYPES.rate , ROOMS.extra_info " +
                "FROM ROOMS " +
                "JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                "WHERE ROOM_TYPES.id = ? " +
                " AND ROOMS.id NOT IN (SELECT ROOMS.id " +
                " FROM ROOMS " +
                " JOIN ROOM_TYPES ON ROOMS.room_type_id = ROOM_TYPES.id " +
                " JOIN RESERVATIONS ON ROOMS.id = RESERVATIONS.room_id " +
                " WHERE end_date <= STRFTIME(?) " +
                " AND ROOM_TYPES.id = ? " +
                " AND RESERVATIONS.status NOT LIKE 'CANCELED');";

        String sql = null;

        if (startDate == null && endDate == null && roomTypeId != null) {
            sql = sqlAllByRoomTypeId;
        } else if (startDate != null && endDate != null) {
            if (roomTypeId == null) {
                sql = sqlAvailableBetween;
            } else {
                sql = sqlAvailableBetweenByType;
            }
        } else if (startDate != null) {
            if (roomTypeId == null) {
                sql = sqlAvailableFromNoType;
            } else {
                sql = sqlAvailableFromByType;
            }
        } else if (endDate != null) {
            if (roomTypeId == null) {
                sql = sqlAvailableUntilNoType;
            } else {
                sql = sqlAvailableUntilByType;
            }
        }
        return sql;
    }
}
