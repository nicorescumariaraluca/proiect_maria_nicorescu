package wantsome.project.db.dto;

public enum Types {

    SINGLE(1),
    DOUBLE(2),
    TWIN(2),
    TRIPLE(3),
    SUITE(4);

    private final int capacity;

    Types(int capacity) {
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public String toString() {
        return "Type{" +
                "capacity=" + capacity +
                '}';
    }
}
