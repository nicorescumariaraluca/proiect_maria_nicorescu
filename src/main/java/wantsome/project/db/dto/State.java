package wantsome.project.db.dto;

public enum State {
    ACTIVE("Active"),
    CANCELED("Canceled"),
    PAID("Paid");

    private final String label;

    State(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
