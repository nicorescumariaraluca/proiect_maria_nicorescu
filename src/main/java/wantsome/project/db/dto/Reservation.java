package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class Reservation {

    private final long id;
    private final long clientId;
    private final Date startDate;
    private final Date endDate;
    private final long roomId;
    private final String extraInfo;
    private final State state;

    public Reservation(long id, long clientId, Date startDate, Date endDate, long roomId, String extraInfo, State state) {
        this.id = id;
        this.clientId = clientId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.roomId = roomId;
        this.extraInfo = extraInfo;
        this.state = state;
    }

    /*
    version without id, needed for the case of new reservation to insert in DB
    when id is not yet set / known
     */

    public Reservation(long clientId, Date startDate, Date endDate, long roomId, String extraInfo, State state) {
        this(-1, clientId, startDate, endDate, roomId, extraInfo, state);
    }

    public long getId() {
        return id;
    }

    public long getClientId() {
        return clientId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public long getRoomId() {
        return roomId;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + id +
                ", clientId=" + clientId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", roomId=" + roomId +
                ", extraInfo='" + extraInfo + '\'' +
                ", status='" + state + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reservation)) return false;
        Reservation that = (Reservation) o;
        return id == that.id &&
                clientId == that.clientId &&
                roomId == that.roomId &&
                startDate.equals(that.startDate) &&
                endDate.equals(that.endDate) &&
                Objects.equals(extraInfo, that.extraInfo) &&
                Objects.equals(state, that.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientId, startDate, endDate, roomId, extraInfo, state);
    }
}
