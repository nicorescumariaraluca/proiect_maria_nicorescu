package wantsome.project.db.dto;

import java.util.Objects;

public class RoomTypes {

    private final long id;
    private final String description;
    private final int rate;
    private final int capacity;

    public RoomTypes(long id, Types description, int rate) {
        this.id = id;
        this.description = description.name();
        this.rate = rate;
        this.capacity = description.getCapacity();
    }

    public RoomTypes(long id, String description, int rate, int capacity) {
        this.id = id;
        this.description = description;
        this.rate = rate;
        this.capacity = capacity;
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public double getRate() {
        return rate;
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public String toString() {
        return "RoomTypes{" +
                "id=" + id +
                ", description=" + description +
                ", rate=" + rate +
                ", capacity=" + capacity +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomTypes)) return false;
        RoomTypes roomTypes = (RoomTypes) o;
        return id == roomTypes.id &&
                rate == roomTypes.rate &&
                capacity == roomTypes.capacity &&
                Objects.equals(description, roomTypes.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, rate, capacity);
    }
}
