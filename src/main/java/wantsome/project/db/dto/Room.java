package wantsome.project.db.dto;

import java.util.Objects;

public class Room {

    private final long id;
    private final long roomTypeId;
    private final String extraInfo;

    public Room(long id, long roomTypeId, String extraInfo) {
        this.id = id;
        this.roomTypeId = roomTypeId;
        this.extraInfo = extraInfo;
    }

    public long getId() {
        return id;
    }

    public long getRoomTypeId() {
        return roomTypeId;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", roomTypeId=" + roomTypeId +
                ", extraInfo='" + extraInfo + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room)) return false;
        Room room = (Room) o;
        return id == room.id &&
                roomTypeId == room.roomTypeId &&
                Objects.equals(extraInfo, room.extraInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomTypeId, extraInfo);
    }
}
