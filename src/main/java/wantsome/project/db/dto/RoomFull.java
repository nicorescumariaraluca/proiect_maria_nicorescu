package wantsome.project.db.dto;

import java.util.Objects;

public class RoomFull {

    private final long id;
    private final long roomTypeId;
    private final String description;
    private final int capacity;
    private final int rate;
    private final String extraInfo;

    public RoomFull(long id, long roomTypeId, String description, int capacity, int rate, String extraInfo) {
        this.id = id;
        this.roomTypeId = roomTypeId;
        this.description = description;
        this.capacity = capacity;
        this.rate = rate;
        this.extraInfo = extraInfo;
    }

    public long getId() {
        return id;
    }

    public long getRoomTypeId() {
        return roomTypeId;
    }

    public String getDescription() {
        return description;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getRate() {
        return rate;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    @Override
    public String toString() {
        return "RoomFull{" +
                "id=" + id +
                ", roomTypeId=" + roomTypeId +
                ", description='" + description + '\'' +
                ", capacity=" + capacity +
                ", rate=" + rate +
                ", extraInfo='" + extraInfo + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomFull)) return false;
        RoomFull roomFull = (RoomFull) o;
        return id == roomFull.id &&
                roomTypeId == roomFull.roomTypeId &&
                capacity == roomFull.capacity &&
                rate == roomFull.rate &&
                description.equals(roomFull.description) &&
                extraInfo.equals(roomFull.extraInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roomTypeId, description, capacity, rate, extraInfo);
    }
}
