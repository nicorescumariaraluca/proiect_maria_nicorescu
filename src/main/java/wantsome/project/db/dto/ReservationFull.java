package wantsome.project.db.dto;

import java.sql.Date;
import java.util.Objects;

public class ReservationFull {

    private final long id;
    private final long clientId;
    private final String firstName;
    private final String lastName;
    private final Date startDate;
    private final Date endDate;
    private final long roomId;
    private final String description;
    private final String extraInfo;
    private final State state;

    public ReservationFull(long id, long clientId, String firstName, String lastName, Date startDate,
                           Date endDate, long roomId, String description, String extraInfo, State state) {
        this.id = id;
        this.clientId = clientId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.roomId = roomId;
        this.description = description;
        this.extraInfo = extraInfo;
        this.state = state;
    }

    public long getId() {
        return id;
    }

    public long getClientId() {
        return clientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public long getRoomId() {
        return roomId;
    }

    public String getDescription() {
        return description;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return "ReservationFull{" +
                "id=" + id +
                ", clientId=" + clientId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", roomId=" + roomId +
                ", description='" + description + '\'' +
                ", extraInfo='" + extraInfo + '\'' +
                ", state=" + state +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReservationFull)) return false;
        ReservationFull that = (ReservationFull) o;
        return id == that.id &&
                clientId == that.clientId &&
                roomId == that.roomId &&
                firstName.equals(that.firstName) &&
                lastName.equals(that.lastName) &&
                startDate.equals(that.startDate) &&
                endDate.equals(that.endDate) &&
                description.equals(that.description) &&
                Objects.equals(extraInfo, that.extraInfo) &&
                state == that.state;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientId, firstName, lastName, startDate, endDate,
                roomId, description, extraInfo, state);
    }
}
