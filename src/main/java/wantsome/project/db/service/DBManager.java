package wantsome.project.db.service;

import org.sqlite.SQLiteConfig;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {

    public final static String JDBC_URL = "jdbc:sqlite:Hotel_Reservation.db";

    public static Connection getConnection() throws SQLException {

        SQLiteConfig config = new SQLiteConfig();
        config.enforceForeignKeys(true); //enable FK support (disabled by default)
        config.setDateStringFormat("yyyy-MM-dd"); //to avoid some problems with date/time fields

        return DriverManager.getConnection(JDBC_URL, config.toProperties());
    }

}
