package wantsome.project.db.service;

import wantsome.project.db.dao.ClientDAO;
import wantsome.project.db.dao.ReservationDAO;
import wantsome.project.db.dao.RoomDAO;
import wantsome.project.db.dao.RoomTypesDAO;
import wantsome.project.db.dto.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;


public class DBInitService {
    /**
     * Initializes the DB as needed - creates missing tables if not exists
     */

    private static final String CREATE_CLIENTS_SQL = "CREATE TABLE IF NOT EXISTS CLIENTS ( " +
            " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            " first_name VARCHAR(100) NOT NULL, " +
            " last_name VARCHAR(100) NOT NULL, " +
            " email VARCHAR(100) NOT NULL UNIQUE, " +
            " address VARCHAR(200) NOT NULL " +
            ");";

    private static final String CREATE_ROOM_TYPES_SQL = "CREATE TABLE IF NOT EXISTS ROOM_TYPES (\n" +
            " id INTEGER NOT NULL PRIMARY KEY, " +
            " description VARCHAR(10) NOT NULL, " +
            " rate INTEGER NOT NULL, " +
            " capacity INTEGER NOT NULL " +
            ");";

    private static final String CREATE_ROOMS_SQL = "CREATE TABLE IF NOT EXISTS ROOMS ( " +
            " id INTEGER NOT NULL PRIMARY KEY, " +
            " room_type_id INTEGER REFERENCES ROOM_TYPES(id) NOT NULL, " +
            " extra_info VARCHAR(200) " +
            ");";

    private static final String CREATE_RESERVATIONS_SQL = "CREATE TABLE IF NOT EXISTS RESERVATIONS ( " +
            " id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            " client_id INTEGER REFERENCES CLIENTS(id) NOT NULL, " +
            " start_date DATE NOT NULL, " +
            " end_date DATE NOT NULL, " +
            " room_id INTEGER REFERENCES ROOMS(id) NOT NULL, " +
            " extra_info VARCHAR(200), " +
            " status VARCHAR(10) NOT NULL, " +
            " CONSTRAINT CHK_STATUS CHECK (status in ('PAID', 'CANCELED', 'ACTIVE')), " +
            " CONSTRAINT CHK_DATE CHECK (end_date >= start_date) " +
            ");";


    public static void initializeDataBase() {

        createMissingTables();

        if (RoomTypesDAO.getAll().isEmpty()) {
            insertInitialRoomTypes();
        }

        if (RoomDAO.getAll().isEmpty()) {
            insertInitialRooms();
        }

        if (ClientDAO.getAll().isEmpty()) {
            insertInitialClients();
        }

        if (ReservationDAO.getAll().isEmpty()) {
            insertInitialReservations();
        }

    }

    /**
     * Using “if not exists” statement when creating tables
     */
    private static void createMissingTables() {

        try (Connection conn = DBManager.getConnection();
             Statement st = conn.createStatement()) {

            st.execute(CREATE_ROOM_TYPES_SQL);
            st.execute(CREATE_ROOMS_SQL);
            st.execute(CREATE_CLIENTS_SQL);
            st.execute(CREATE_RESERVATIONS_SQL);

        } catch (SQLException e) {
            System.err.println("Error creating missing tables: " + e.getMessage());
        }
    }

    /**
     * Method to insert initial data in table ROOM_TYPES
     */
    private static void insertInitialRoomTypes() {

        RoomTypesDAO.insert(new RoomTypes(1, Types.SINGLE.name(), 200, Types.SINGLE.getCapacity()));
        RoomTypesDAO.insert(new RoomTypes(2, Types.DOUBLE.name(), 450, Types.DOUBLE.getCapacity()));
        RoomTypesDAO.insert(new RoomTypes(3, Types.TWIN.name(), 350, Types.TWIN.getCapacity()));
        RoomTypesDAO.insert(new RoomTypes(4, Types.TRIPLE.name(), 550, Types.TRIPLE.getCapacity()));
        RoomTypesDAO.insert(new RoomTypes(5, Types.SUITE.name(), 900, Types.SUITE.getCapacity()));

    }

    /**
     * Method to insert initial data in table ROOMS
     */
    private static void insertInitialRooms() {

        RoomDAO.insert(new Room(101, 1, "First floor garden view"));
        RoomDAO.insert(new Room(102, 1, "First floor sea view"));
        RoomDAO.insert(new Room(103, 3, "First floor garden view"));
        RoomDAO.insert(new Room(104, 3, "First floor sea view"));
        RoomDAO.insert(new Room(105, 2, "First floor garden view"));
        RoomDAO.insert(new Room(106, 2, "First floor garden view"));
        RoomDAO.insert(new Room(107, 2, "First floor sea view"));
        RoomDAO.insert(new Room(108, 2, "First floor sea view"));
        RoomDAO.insert(new Room(109, 2, "First floor sea view"));
        RoomDAO.insert(new Room(110, 2, "First floor sea view"));
        RoomDAO.insert(new Room(111, 4, "First floor garden view"));
        RoomDAO.insert(new Room(112, 4, "First floor garden view"));
        RoomDAO.insert(new Room(113, 4, "First floor sea view"));
        RoomDAO.insert(new Room(114, 5, "First floor sea view"));

        RoomDAO.insert(new Room(201, 1, "Second floor garden view"));
        RoomDAO.insert(new Room(202, 1, "Second floor sea view"));
        RoomDAO.insert(new Room(203, 3, "Second floor garden view"));
        RoomDAO.insert(new Room(204, 3, "Second floor sea view"));
        RoomDAO.insert(new Room(205, 2, "Second floor garden view"));
        RoomDAO.insert(new Room(206, 2, "Second floor garden view"));
        RoomDAO.insert(new Room(207, 2, "Second floor sea view"));
        RoomDAO.insert(new Room(208, 2, "Second floor sea view"));
        RoomDAO.insert(new Room(209, 2, "Second floor sea view"));
        RoomDAO.insert(new Room(210, 2, "Second floor sea view"));
        RoomDAO.insert(new Room(211, 4, "Second floor garden view"));
        RoomDAO.insert(new Room(212, 4, "Second floor garden view"));
        RoomDAO.insert(new Room(213, 4, "Second floor sea view"));
        RoomDAO.insert(new Room(214, 5, "Second floor sea view"));

        RoomDAO.insert(new Room(301, 1, "Third floor garden view"));
        RoomDAO.insert(new Room(302, 1, "Third floor sea view"));
        RoomDAO.insert(new Room(303, 3, "Third floor garden view"));
        RoomDAO.insert(new Room(304, 3, "Third floor sea view"));
        RoomDAO.insert(new Room(305, 2, "Third floor garden view"));
        RoomDAO.insert(new Room(306, 2, "Third floor garden view"));
        RoomDAO.insert(new Room(307, 2, "Third floor sea view"));
        RoomDAO.insert(new Room(308, 2, "Third floor sea view"));
        RoomDAO.insert(new Room(309, 2, "Third floor sea view"));
        RoomDAO.insert(new Room(310, 2, "Third floor sea view"));
        RoomDAO.insert(new Room(311, 4, "Third floor garden view"));
        RoomDAO.insert(new Room(312, 4, "Third floor garden view"));
        RoomDAO.insert(new Room(313, 4, "Third floor sea view"));
        RoomDAO.insert(new Room(314, 5, "Third floor sea view"));

    }

    /**
     * Method to insert initial data in table CLIENTS
     */
    private static void insertInitialClients() {

        ClientDAO.insert(new Client("Steven", "King", "stevan.king@gmail.com", "London"));
        ClientDAO.insert(new Client("Neena", "Kochhar", "nennak@gmail.com", "London"));
        ClientDAO.insert(new Client("David", "Austin", "david.austin@gmail.com", "London"));
        ClientDAO.insert(new Client("Diana", "Lorentz", "dlorentz@gmail.com", "New York"));
        ClientDAO.insert(new Client("Nancy", "Greenberg", "ngreen@gmail.com", "New York"));
        ClientDAO.insert(new Client("John", "Chen", "johnc@gmail.com", "New York"));
        ClientDAO.insert(new Client("Daniel", "Faviet", "dfaviet@gmail.com", "New York"));
        ClientDAO.insert(new Client("Ismael", "Sciarra", "isciarra@gmail.com", "Milan"));
        ClientDAO.insert(new Client("Luis", "Popp", "luisp@gmail.com", "Milan"));
        ClientDAO.insert(new Client("Alexander", "Khoo", "alexanderk@gmail.com", "Barcelona"));

        ClientDAO.insert(new Client("Karen", "Colmenares", "kakrenc@gmail.com", "Barcelona"));
        ClientDAO.insert(new Client("Matthew", "Weiss", "mattheww@gmail.com", "Barcelona"));
        ClientDAO.insert(new Client("Adam", "Fripp", "adam.fripp@gmail.com", "Barcelona"));
        ClientDAO.insert(new Client("Kevin", "Mourgos", "kevinm@gmail.com", "Bucharest"));
        ClientDAO.insert(new Client("Julia", "Nayer", "jnayer@gmail.com", "Bucharest"));
        ClientDAO.insert(new Client("Irene", "Mikkilineni", "irenem@gmail.com", "Bucharest"));
        ClientDAO.insert(new Client("James", "Landry", "jamesl@gmail.com", "Bucharest"));
        ClientDAO.insert(new Client("Steven", "Markle", "stevenm@gmail.com", "Bucharest"));
        ClientDAO.insert(new Client("Laura", "Bissot", "laura.bissot@gmail.com", "Bucharest"));
        ClientDAO.insert(new Client("James", "Marlow", "jmarlow@gmail.com", "Bucharest"));

        ClientDAO.insert(new Client("Jason", "Mallin", "jasonmallin@gmail.com", "Toronto"));
        ClientDAO.insert(new Client("Michael", "Rogers", "michael.rogers@gmail.com", "Tokyo"));
        ClientDAO.insert(new Client("Stephan", "Stiles", "stephans@gmail.com", "Tokyo"));
        ClientDAO.insert(new Client("John", "Seo", "john.seo@gmail.com", "Tokyo"));
        ClientDAO.insert(new Client("Joshua", "Patel", "jpatel@gmail.com", "Tokyo"));
        ClientDAO.insert(new Client("Curtis", "Davies", "cdavies@gmail.com", "Venice"));
        ClientDAO.insert(new Client("Randall", "Matos", "rmatos@gmail.com", "Venice"));
        ClientDAO.insert(new Client("Peter", "Vargas", "peter.vargas@gmail.com", "Venice"));
        ClientDAO.insert(new Client("John", "Russell", "john.russell@gmail.com", "Venice"));
        ClientDAO.insert(new Client("Karen", "Partners", "kpartners@gmail.com", "Venice"));

        ClientDAO.insert(new Client("Alberto", "Errazuriz", "albertoe@gmail.com", "Geneva"));
        ClientDAO.insert(new Client("Peter", "Tucker", "petert@gmail.com", "Mexico City"));
        ClientDAO.insert(new Client("David", "Bernstein", "davidb@gmail.com", "Mexico City"));
        ClientDAO.insert(new Client("Peter", "Hall", "peterh@gmail.com", "Iasi"));
        ClientDAO.insert(new Client("Nanette", "Cambrault", "nanettec@gmail.com", "Iasi"));
        ClientDAO.insert(new Client("Janette", "King", "janettek@gmail.com", "Iasi"));
        ClientDAO.insert(new Client("Allan", "McEwen", "allanmc@gmail.com", "Iasi"));
        ClientDAO.insert(new Client("Lindsey", "Smith", "lindsey.smith@gmail.com", "Iasi"));
        ClientDAO.insert(new Client("Sarath", "Sewall", "saraths@gmail.com", "Iasi"));
        ClientDAO.insert(new Client("Danielle", "Greene", "danielleg@gmail.com", "Iasi"));

    }

    /**
     * Method to insert initial data in table RESERVATIONS
     */
    private static void insertInitialReservations() {

        ReservationDAO.insert(new Reservation(1, Date.valueOf("2022-01-07"), Date.valueOf("2022-01-11"), 101, "", State.PAID));
        ReservationDAO.insert(new Reservation(2, Date.valueOf("2022-01-07"), Date.valueOf("2022-01-11"), 102, "", State.PAID));
        ReservationDAO.insert(new Reservation(3, Date.valueOf("2022-01-07"), Date.valueOf("2022-01-11"), 103, "", State.PAID));
        ReservationDAO.insert(new Reservation(4, Date.valueOf("2022-01-07"), Date.valueOf("2022-01-11"), 104, "", State.PAID));
        ReservationDAO.insert(new Reservation(5, Date.valueOf("2022-01-14"), Date.valueOf("2022-01-18"), 105, "", State.PAID));
        ReservationDAO.insert(new Reservation(6, Date.valueOf("2022-01-14"), Date.valueOf("2022-01-18"), 106, "", State.PAID));
        ReservationDAO.insert(new Reservation(7, Date.valueOf("2022-01-14"), Date.valueOf("2022-01-18"), 107, "", State.PAID));
        ReservationDAO.insert(new Reservation(8, Date.valueOf("2022-01-21"), Date.valueOf("2022-01-30"), 108, "", State.PAID));
        ReservationDAO.insert(new Reservation(9, Date.valueOf("2022-01-21"), Date.valueOf("2022-01-30"), 109, "", State.PAID));
        ReservationDAO.insert(new Reservation(10, Date.valueOf("2022-01-21"), Date.valueOf("2022-01-30"), 110, "", State.PAID));
        ReservationDAO.insert(new Reservation(11, Date.valueOf("2022-01-21"), Date.valueOf("2022-01-30"), 112, "", State.PAID));
        ReservationDAO.insert(new Reservation(12, Date.valueOf("2022-02-04"), Date.valueOf("2022-02-15"), 113, "", State.PAID));
        ReservationDAO.insert(new Reservation(13, Date.valueOf("2022-02-04"), Date.valueOf("2022-02-15"), 114, "", State.PAID));
        ReservationDAO.insert(new Reservation(14, Date.valueOf("2022-02-04"), Date.valueOf("2022-02-15"), 101, "", State.PAID));
        ReservationDAO.insert(new Reservation(15, Date.valueOf("2022-02-04"), Date.valueOf("2022-02-15"), 102, "", State.CANCELED));
        ReservationDAO.insert(new Reservation(16, Date.valueOf("2022-02-04"), Date.valueOf("2022-02-15"), 103, "", State.PAID));
        ReservationDAO.insert(new Reservation(17, Date.valueOf("2022-02-04"), Date.valueOf("2022-02-15"), 104, "", State.PAID));
        ReservationDAO.insert(new Reservation(18, Date.valueOf("2022-03-11"), Date.valueOf("2022-03-20"), 105, "", State.PAID));
        ReservationDAO.insert(new Reservation(19, Date.valueOf("2022-03-11"), Date.valueOf("2022-03-20"), 106, "", State.CANCELED));
        ReservationDAO.insert(new Reservation(20, Date.valueOf("2022-03-11"), Date.valueOf("2022-03-20"), 107, "", State.PAID));
        ReservationDAO.insert(new Reservation(21, Date.valueOf("2022-04-05"), Date.valueOf("2022-04-27"), 108, "", State.PAID));
        ReservationDAO.insert(new Reservation(22, Date.valueOf("2022-04-05"), Date.valueOf("2022-04-27"), 109, "", State.PAID));
        ReservationDAO.insert(new Reservation(23, Date.valueOf("2022-04-05"), Date.valueOf("2022-04-27"), 110, "", State.PAID));
        ReservationDAO.insert(new Reservation(24, Date.valueOf("2022-04-05"), Date.valueOf("2022-04-27"), 111, "", State.PAID));

        ReservationDAO.insert(new Reservation(25, Date.valueOf("2022-04-29"), Date.valueOf("2022-05-08"), 112, "", State.PAID));
        ReservationDAO.insert(new Reservation(26, Date.valueOf("2022-04-29"), Date.valueOf("2022-05-08"), 113, "", State.PAID));
        ReservationDAO.insert(new Reservation(27, Date.valueOf("2022-05-16"), Date.valueOf("2022-05-29"), 114, "", State.PAID));
        ReservationDAO.insert(new Reservation(28, Date.valueOf("2022-05-16"), Date.valueOf("2022-05-29"), 201, "", State.PAID));
        ReservationDAO.insert(new Reservation(29, Date.valueOf("2022-05-16"), Date.valueOf("2022-05-29"), 202, "", State.PAID));
        ReservationDAO.insert(new Reservation(30, Date.valueOf("2022-05-16"), Date.valueOf("2022-05-29"), 203, "", State.PAID));
        ReservationDAO.insert(new Reservation(31, Date.valueOf("2022-05-16"), Date.valueOf("2022-05-29"), 204, "", State.PAID));
        ReservationDAO.insert(new Reservation(32, Date.valueOf("2022-05-16"), Date.valueOf("2022-05-29"), 206, "", State.CANCELED));
        ReservationDAO.insert(new Reservation(33, Date.valueOf("2022-05-16"), Date.valueOf("2022-05-29"), 207, "", State.PAID));
        ReservationDAO.insert(new Reservation(34, Date.valueOf("2022-05-16"), Date.valueOf("2022-05-29"), 208, "", State.PAID));
        ReservationDAO.insert(new Reservation(35, Date.valueOf("2022-05-16"), Date.valueOf("2022-05-29"), 209, "", State.PAID));
        ReservationDAO.insert(new Reservation(36, Date.valueOf("2022-05-30"), Date.valueOf("2022-06-09"), 210, "", State.PAID));
        ReservationDAO.insert(new Reservation(37, Date.valueOf("2022-05-30"), Date.valueOf("2022-06-09"), 211, "", State.PAID));
        ReservationDAO.insert(new Reservation(38, Date.valueOf("2022-05-30"), Date.valueOf("2022-06-09"), 212, "", State.PAID));

        ReservationDAO.insert(new Reservation(39, Date.valueOf("2022-06-10"), Date.valueOf("2022-06-18"), 213, "", State.PAID));
        ReservationDAO.insert(new Reservation(40, Date.valueOf("2022-06-10"), Date.valueOf("2022-06-18"), 214, "", State.PAID));
        ReservationDAO.insert(new Reservation(1, Date.valueOf("2022-06-10"), Date.valueOf("2022-06-18"), 101, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(2, Date.valueOf("2022-06-10"), Date.valueOf("2022-06-18"), 102, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(3, Date.valueOf("2022-06-10"), Date.valueOf("2022-06-18"), 103, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(4, Date.valueOf("2022-06-10"), Date.valueOf("2022-06-18"), 104, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(5, Date.valueOf("2022-06-10"), Date.valueOf("2022-06-18"), 105, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(6, Date.valueOf("2022-06-10"), Date.valueOf("2022-06-18"), 106, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(7, Date.valueOf("2022-06-17"), Date.valueOf("2022-06-30"), 107, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(8, Date.valueOf("2022-06-17"), Date.valueOf("2022-06-30"), 108, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(9, Date.valueOf("2022-06-17"), Date.valueOf("2022-06-30"), 109, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(10, Date.valueOf("2022-06-17"), Date.valueOf("2022-06-30"), 110, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(11, Date.valueOf("2022-06-17"), Date.valueOf("2022-06-30"), 111, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(12, Date.valueOf("2022-06-17"), Date.valueOf("2022-06-30"), 112, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(13, Date.valueOf("2022-06-17"), Date.valueOf("2022-06-30"), 113, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(14, Date.valueOf("2022-06-17"), Date.valueOf("2022-06-30"), 114, "", State.ACTIVE));

        ReservationDAO.insert(new Reservation(15, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 201, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(16, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 202, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(17, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 203, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(18, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 204, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(19, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 205, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(20, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 206, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(21, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 207, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(22, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 208, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(23, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 209, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(24, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 210, "", State.CANCELED));
        ReservationDAO.insert(new Reservation(25, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 211, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(26, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 212, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(27, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 213, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(28, Date.valueOf("2022-07-01"), Date.valueOf("2022-07-31"), 214, "", State.ACTIVE));

        ReservationDAO.insert(new Reservation(29, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 101, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(30, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 102, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(31, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 103, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(32, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 104, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(33, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 105, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(34, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 106, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(35, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 107, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(36, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 108, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(37, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 109, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(38, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 110, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(39, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 111, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(40, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 112, "", State.CANCELED));
        ReservationDAO.insert(new Reservation(1, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 113, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(2, Date.valueOf("2022-08-01"), Date.valueOf("2022-08-31"), 114, "", State.ACTIVE));

        ReservationDAO.insert(new Reservation(3, Date.valueOf("2022-09-01"), Date.valueOf("2022-09-30"), 101, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(4, Date.valueOf("2022-09-01"), Date.valueOf("2022-09-30"), 102, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(5, Date.valueOf("2022-09-01"), Date.valueOf("2022-09-30"), 103, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(6, Date.valueOf("2022-09-01"), Date.valueOf("2022-09-30"), 104, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(7, Date.valueOf("2022-09-01"), Date.valueOf("2022-09-30"), 105, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(8, Date.valueOf("2022-10-14"), Date.valueOf("2022-10-30"), 106, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(9, Date.valueOf("2022-10-14"), Date.valueOf("2022-10-30"), 107, "", State.CANCELED));
        ReservationDAO.insert(new Reservation(10, Date.valueOf("2022-10-14"), Date.valueOf("2022-10-30"), 108, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(11, Date.valueOf("2022-10-14"), Date.valueOf("2022-10-30"), 109, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(12, Date.valueOf("2022-11-12"), Date.valueOf("2022-11-20"), 110, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(13, Date.valueOf("2022-11-12"), Date.valueOf("2022-11-20"), 111, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(14, Date.valueOf("2022-11-12"), Date.valueOf("2022-11-20"), 112, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(15, Date.valueOf("2022-11-12"), Date.valueOf("2022-11-20"), 113, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(16, Date.valueOf("2022-12-11"), Date.valueOf("2022-12-18"), 114, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(17, Date.valueOf("2022-12-11"), Date.valueOf("2022-12-18"), 101, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(18, Date.valueOf("2022-12-11"), Date.valueOf("2022-12-18"), 102, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(19, Date.valueOf("2022-12-11"), Date.valueOf("2022-12-18"), 103, "", State.ACTIVE));
        ReservationDAO.insert(new Reservation(20, Date.valueOf("2022-12-11"), Date.valueOf("2022-12-19"), 104, "", State.ACTIVE));

    }

}
