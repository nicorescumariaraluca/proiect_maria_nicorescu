package wantsome.project.db.service;

import wantsome.project.db.dao.ClientDAO;
import wantsome.project.db.dao.ReservationDAO;
import wantsome.project.db.dao.RoomDAO;
import wantsome.project.db.dao.RoomTypesDAO;
import wantsome.project.db.dto.Client;
import wantsome.project.db.dto.Reservation;
import wantsome.project.db.dto.Room;
import wantsome.project.db.dto.RoomTypes;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Invoice {

    /**
     * Set Summer Season and Cold Season for Hotel and differentiated prices
     * Summer Season: from 21st day of June until 23rd day of September, rate + 20%, +35% for sea view
     * Cold Season: from 1st of November until 1st day of March, rate - 10% for all rooms
     */
    public final static int START_SUMMER_SEASON_MONTH = 5; //5 to compute June (6)
    public final static int START_SUMMER_SEASON_DAY = 21;
    public final static int END_SUMMER_SEASON_MONTH = 8;  //8 to compute September (9)
    public final static int END_SUMMER_SEASON_DAY = 23;

    public final static int START_COLD_SEASON_MONTH = 10; //10 to compute November (11)
    public final static int START_COLD_SEASON_DAY = 1;
    public final static int END_COLD_SEASON_MONTH = 2;  //2 to compute March (3)
    public final static int END_COLD_SEASON_DAY = 1;

    public final static double RATE_SEA_VIEW = 35;
    public final static double RATE_SUMMER = 20;
    public final static double RATE_COLD = 10;


    /**
     * Compute invoice for reservation by client id
     */
    public static String getInvoiceById(long id) {

        Reservation reservation = ReservationDAO.getById(id).orElse(null);

        assert reservation != null;
        Client client = ClientDAO.getById(reservation.getClientId()).orElse(null);
        assert client != null;
        String clientName = client.getFirstName() + " " + client.getLastName();

        Date startDate = reservation.getStartDate();
        Date endDate = reservation.getEndDate();
        long days = getDaysBetween(startDate, endDate);

        long roomId = reservation.getRoomId();

        String typeOfRoom = getRoomType(roomId).getDescription();
        int capacity = getRoomType(roomId).getCapacity();

        double standardRate = getRoomType(roomId).getRate();
        double totalPrice = getTotalPrice(startDate, endDate, roomId, standardRate);
        double pricePerDay = totalPrice / days;

        return
                "<pre>" + "                                     INVOICE                                 " + "\n" + "<br>" +
                        "                                                                             " + "\n" + "<br>" +
                        "     THALASSA HOTEL                                   BILLED TO " + clientName + "\n" + "<br>" +
                        "     Thasos, Greece                                   " + client.getAddress() + "\n" + "<br>" +
                        "     thalassa.hotel@gmail.com                         " + client.getEmail() + "\n" + "<br>" +
                        "                                                                                  " + "\n" + "<br>" +
                        "                                                                                  " + "\n" + "<br>" +
                        "     Reservation id: " + reservation.getId() + "\n" + "<br>" +
                        "     Period of reservation: " + startDate + " ~ " + endDate + "\n" + "<br>" +
                        "                                                                                 " + "\n" + "<br>" +
                        "     ---------------------------------------------------------------------------" + "\n" + "<br>" +
                        "          Type of           Capacity          Period           Price / day   " + "\n" + "<br>" +
                        "           room             (person)          (days)              (RON)  " + "\n" + "<br>" +
                        "     ---------------------------------------------------------------------------" + "\n" + "<br>" +
                        "           " + typeOfRoom + "               " + capacity + "                 " + days + "               " + String.format("%,.2f", pricePerDay) + " \n" + "<br>" +
                        "     ---------------------------------------------------------------------------" + "\n" + "<br>" +
                        "                                             Total price (RON):  " + String.format("%,.2f", totalPrice) + " </pre>";

    }

    /**
     * Days in Summer Season > 0, no days in Cold Season, Room with sea view, possibilities:
     * 1. all days in summer Season with RATE_SEA_VIEW;
     * 2. some days in Summer Season and some of them with standard rate (mixed rates);
     * <p>
     * Days in Summer Season > 0, no days in Cold Season, Room with garden view, possibilities:
     * 1. all days in Summer Season with RATE_SUMMER;
     * 2. some days in Summer Season and some of them with standard rate (mixed rates);
     * <p>
     * Days in Cold Season > 0, no days in Summer Season, no condition for view, possibilities:
     * 1. all days in Cold Season with RATE_COLD;
     * 2. some days in Cold Season and some of them with standard rate (mixed rates);
     * <p>
     * Days in Summer Season and Cold Season > 0, possibilities, (mixed rate):
     * 1. Room with sea view:
     * for days in Summer Season: RATE_SEA_VIEW;
     * for days in Cold Season: RATE_COLD;
     * for the rest of days: standard rate;
     * <p>
     * 2. Room with garden view:
     * for days in Summer Season: RATE_SUMMER;
     * for days in Cold Season: RATE_COLD;
     * for the rest of days: standard rate;
     * <p>
     * Period between Seasons, Summer && Cold = 0
     */

    public static double getTotalPrice(Date startDate, Date endDate, long roomId, double standardRate) {

        double rate = 0;

        long daysReservation = getDaysBetween(startDate, endDate);
        long daysSummerSeason = getDaysSummerSeason(startDate, endDate);
        long daysColdSeason = getDaysColdSeason(startDate, endDate);

        double summerSeaRate = (standardRate + (standardRate * RATE_SEA_VIEW / 100)) * daysSummerSeason;
        double summerRate = (standardRate + (standardRate * RATE_SUMMER / 100)) * daysSummerSeason;
        double coldRate = (standardRate - (standardRate * RATE_COLD / 100)) * daysColdSeason;

        if (daysSummerSeason > 0 && daysColdSeason == 0 && hasSeaView(roomId)) {

            if (daysSummerSeason == daysReservation) {
                rate = (standardRate + (standardRate * RATE_SEA_VIEW / 100)) * daysReservation;
            } else {
                double notSeasonRate = standardRate * (daysReservation - daysSummerSeason);
                rate = summerSeaRate + notSeasonRate;
            }

        } else if (daysSummerSeason > 0 && daysColdSeason == 0 && !hasSeaView(roomId)) {

            if (daysSummerSeason == daysReservation) {
                rate = (standardRate + (standardRate * RATE_SUMMER / 100)) * daysReservation;
            } else {
                double notSeasonRate = standardRate * (daysReservation - daysSummerSeason);
                rate = summerRate + notSeasonRate;
            }

        } else if (daysSummerSeason == 0 && daysColdSeason > 0) {

            if (daysColdSeason == daysReservation) {
                rate = (standardRate - (standardRate * RATE_COLD / 100)) * daysReservation;
            } else {
                double notSeasonRate = standardRate * (daysReservation - daysColdSeason);
                rate = coldRate + notSeasonRate;
            }

        } else if (daysSummerSeason > 0 && daysColdSeason > 0) {

            double notSeasonRate = standardRate * (daysReservation - daysSummerSeason - daysColdSeason);

            if (hasSeaView(roomId)) {
                rate = coldRate + summerSeaRate + notSeasonRate;
            } else {
                rate = coldRate + summerRate + notSeasonRate;
            }

        } else if (daysSummerSeason == 0 && daysColdSeason == 0) {
            rate = standardRate * daysReservation;
        }

        return rate;
    }

    /**
     * Get the type of room by room id
     */
    public static RoomTypes getRoomType(long roomId) {

        Room room = RoomDAO.getById(roomId).orElse(null);

        assert room != null;
        long roomTypeId = room.getRoomTypeId();

        return RoomTypesDAO.getById(roomTypeId).orElse(null);
    }

    /**
     * Check if a room has sea view or not (by room id)
     */
    public static boolean hasSeaView(long roomId) {

        Room room = RoomDAO.getById(roomId).orElse(null);
        assert room != null;
        String extraInfo = room.getExtraInfo();

        List<String> words = new ArrayList<>(Arrays.asList(extraInfo.split(" ")));

        return words.contains("sea");
    }

    /**
     * Compute the number of days between dates (for a reservation)
     */
    public static long getDaysBetween(Date startDate, Date endDate) {

        long differenceInTime = endDate.getTime() - startDate.getTime();

        return (differenceInTime / 1000 / 60 / 60 / 24);
    }

    /**
     * Compute the number of days in Summer Season for a reservation (21st of June -> 23rd of September)
     */
    public static long getDaysSummerSeason(Date startDate, Date endDate) {

        //Summer season in application: from 21st of June until 23rd of September
        int actualStartYear = startDate.getYear();
        int actualStopYear = endDate.getYear();

        Date actualStartSummerSeason = new Date(actualStartYear, START_SUMMER_SEASON_MONTH, START_SUMMER_SEASON_DAY);
        Date actualStopSummerSeason = new Date(actualStopYear, END_SUMMER_SEASON_MONTH, END_SUMMER_SEASON_DAY);

        long daysSummerSeason = -1;

        long diffStart = getDaysBetween(startDate, actualStartSummerSeason); // > 0 -> not in SS, <= 0 -> in SS
        long diffStop = getDaysBetween(endDate, actualStopSummerSeason); // >= 0 -> in SS, < 0 -> not in SS
        long diffStopBefore = getDaysBetween(endDate, actualStartSummerSeason); // > 0 -> not in SS, <= 0 -> in SS
        long diffStartAfter = getDaysBetween(startDate, actualStopSummerSeason); // < 0 -> not in SS, <= 0 -> in SS

        // both startDate & endDate in Summer Season
        if (diffStart <= 0 && diffStop >= 0) {
            daysSummerSeason = getDaysBetween(startDate, endDate);
        }
        // both startDate & endDate before Summer Season OR after Summer Season
        else if ((diffStart > 0 && diffStopBefore > 0) || (diffStartAfter < 0 && diffStop < 0)) {
            daysSummerSeason = 0;
        }
        //startDate before, endDate in Summer Season
        else if (diffStart > 0 && diffStop >= 0) {
            daysSummerSeason = getDaysBetween(actualStartSummerSeason, endDate);
        }
        //startDate in Summer Season, endDate after
        else if (diffStart <= 0) {
            daysSummerSeason = getDaysBetween(startDate, actualStopSummerSeason);
        }

        return daysSummerSeason;
    }

    /**
     * Compute the number of days in Cold Season for a reservation (1st of November -> 1st of March)
     */
    public static long getDaysColdSeason(Date startDate, Date endDate) {

        //Cold season in application: from 1st of November until 28th of February
        //Summer season in application: from 21st of June until 23rd of September
        int actualStartYear = startDate.getYear();

        int actualStopYear = endDate.getYear();

        Date actualStartColdSeason = new Date(actualStartYear, START_COLD_SEASON_MONTH, START_COLD_SEASON_DAY);
        Date actualStopColdSeason = new Date(actualStopYear + 1, END_COLD_SEASON_MONTH, END_COLD_SEASON_DAY);

        long daysColdSeason = -1;

        long diffStart = getDaysBetween(startDate, actualStartColdSeason); // > 0 -> not in CS, <= 0 -> in CS
        long diffStop = getDaysBetween(endDate, actualStopColdSeason); // >= 0 -> in CS, < 0 -> not in CS
        long diffStopBefore = getDaysBetween(endDate, actualStartColdSeason); // > 0 -> not in CS, <= 0 -> in CS
        long diffStartAfter = getDaysBetween(startDate, actualStopColdSeason); // < 0 -> not in CS, <= 0 -> in CS

        // both startDate & endDate in Cold Season
        if (diffStart <= 0 && diffStop >= 0) {
            daysColdSeason = getDaysBetween(startDate, endDate);
        }
        // both startDate & endDate before Cold Season OR after Cold Season
        else if ((diffStart > 0 && diffStopBefore > 0) || (diffStartAfter < 0 && diffStop < 0)) {
            daysColdSeason = 0;
        }
        //startDate before, endDate in Cold Season
        else if (diffStart > 0 && diffStop >= 0) {
            daysColdSeason = getDaysBetween(actualStartColdSeason, endDate);
        }
        //startDate in Summer Season, endDate after
        else if (diffStart <= 0) {
            daysColdSeason = getDaysBetween(startDate, actualStopColdSeason);
        }

        return daysColdSeason;
    }

}
