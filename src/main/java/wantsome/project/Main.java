package wantsome.project;

import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wantsome.project.db.service.DBInitService;
import wantsome.project.ui.web.clients.AddEditClientsController;
import wantsome.project.ui.web.clients.ClientsController;
import wantsome.project.ui.web.other.ErrorController;
import wantsome.project.ui.web.other.HomeController;
import wantsome.project.ui.web.other.ReportsController;
import wantsome.project.ui.web.reservations.AddEditReservationController;
import wantsome.project.ui.web.reservations.InvoiceController;
import wantsome.project.ui.web.reservations.ReservationsController;
import wantsome.project.ui.web.rooms.AddEditRoomsController;
import wantsome.project.ui.web.rooms.RoomsController;

/**
 * Main class for web application (using Javalin framework)
 */
public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        setup();
        startWebServer();

    }

    private static void setup() {
        DBInitService.initializeDataBase();
    }

    private static void startWebServer() {
        Javalin app = Javalin.create(config -> {
            config.addStaticFiles("/public", Location.CLASSPATH); //location of static resources (like images, .css ..), relative to /resources dir
            config.enableDevLogging(); //extra logging, for development/debug
        }).start(); ///using default port; to use a specific port: '.start(8080)' (or '0' for random port)

        //set default page
        app.get("/main", HomeController::showHomePage);
        app.get("/", ctx -> ctx.redirect("/main"));

        //--- ROOMS ---//
        app.get("/rooms", RoomsController::showRoomsPage);
        app.get("/rooms/delete/{id}", RoomsController::handleDeleteRequest);

        app.get("/rooms/add", AddEditRoomsController::showAddForm);
        app.post("/rooms/add/{id}", AddEditRoomsController::handleAddRequest);
        app.get("/rooms/update/{id}", AddEditRoomsController::showUpdateForm);
        app.post("/rooms/update/{id}", AddEditRoomsController::handleUpdateRequest);

        //--- CLIENTS ---//
        app.get("/clients", ClientsController::showClientsPage);
        app.get("/clients/delete/{id}", ClientsController::handleDeleteRequest);
        app.get("/clients/add", AddEditClientsController::showAddForm);
        app.post("/clients/add", AddEditClientsController::handleAddUpdatePostRequest);
        app.get("/clients/update/{id}", AddEditClientsController::showUpdateForm);
        app.post("/clients/update/{id}", AddEditClientsController::handleAddUpdatePostRequest);

        //actions on reservations from page CLIENTS
        app.get("/client/{id}", ReservationsController::showReservationsForClient);
        app.get("/client/add/{id}", AddEditReservationController::showAddFormForClient);
        app.post("/client/add/{id}", AddEditReservationController::handleAddReservationForClientPostRequest);

        //--- RESERVATIONS ---//
        app.get("/reservations", ReservationsController::showReservationsPage);
        app.get("/reservations/delete/{id}", ReservationsController::handleDeleteRequest);

        app.get("/invoice/{id}", InvoiceController::showInvoice);

        app.get("/reservations/update/{id}", AddEditReservationController::showUpdateForm);
        app.post("/reservations/update/{id}", AddEditReservationController::handleUpdatePostRequest);

        //--- REPORTS ---//
        app.get("/reports", ReportsController::showReportsPage);

        //--- Error handling ---//
        app.exception(Exception.class, ErrorController::handleException);

        logger.info("Server started: http://localhost:" + app.port());
    }
}
