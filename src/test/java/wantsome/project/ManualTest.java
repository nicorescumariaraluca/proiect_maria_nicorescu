package wantsome.project;

import wantsome.project.db.dao.ClientDAO;
import wantsome.project.db.dao.ReservationDAO;
import wantsome.project.db.dao.RoomDAO;
import wantsome.project.db.dao.RoomTypesDAO;
import wantsome.project.db.dto.*;
import wantsome.project.db.service.Invoice;

import java.sql.Date;
import java.util.List;

public class ManualTest {

    public static void main(String[] args) {


        /**
         * Some manual tests for Objects created:
         */

        Client client = new Client(2, "Raluca", "Nicorescu", "@gmail.com", "Neamt");

        RoomTypes roomType1 = new RoomTypes(1, Types.SINGLE.name(), 200, Types.SINGLE.getCapacity());
        RoomTypes roomType2 = new RoomTypes(2, Types.DOUBLE.name(), 450, Types.DOUBLE.getCapacity());
        RoomTypes roomType3 = new RoomTypes(3, Types.TWIN.name(), 350, Types.TWIN.getCapacity());
        RoomTypes roomType4 = new RoomTypes(4, Types.TRIPLE.name(), 550, Types.TRIPLE.getCapacity());
        RoomTypes roomType5 = new RoomTypes(5, Types.SUITE.name(), 900, Types.SUITE.getCapacity());

        Room room1 = new Room(4, roomType2.getId(), "Sea view");
        Room room2 = new Room(3, roomType5.getId(), null);
        State state = State.ACTIVE;

        Reservation reservation = new Reservation(1, client.getId(), java.sql.Date.valueOf("2022-09-01"),
                java.sql.Date.valueOf("2022-09-05"), room1.getId(), "Sea view", state);

        System.out.println(client);
        System.out.println(reservation);
        System.out.println(room1);
        System.out.println(room2);
        System.out.println(roomType1 + "\n" + roomType2 + "\n" + roomType3 + "\n" + roomType4 + "\n" + roomType5);


        /**
         * Some manual test to get / insert / update / delete clients from / in DB:
         */
        Client client1 = new Client("Raluca", "Nicorescu", "r@gmail.com", "Neamt");
        Client client2 = new Client("Alex", "Popovici", "a@gmail.com", "Neamt");

        List<Client> clients = ClientDAO.getAll();
        System.out.println("Number of current clients: " + clients.size());
        clients.forEach(System.out::println);

        ClientDAO.insert(client1);
        ClientDAO.insert(client2);

        System.out.println("Number of current clients: " + clients.size());

        clients = ClientDAO.getAll();
        client1 = clients.stream()
                .filter(i -> i.getFirstName().equalsIgnoreCase("Raluca"))
                .findFirst().orElse(null);
        System.out.println("client1 inserted with id: " + client1);

        clients = ClientDAO.getAll();
        client2 = clients.stream()
                .filter(i -> i.getFirstName().equalsIgnoreCase("Alex"))
                .findFirst().orElse(null);
        System.out.println("client2 inserted with id: " + client2);

        System.out.println(ClientDAO.getById(40));

        assert client1 != null;
        Client updatedClient1 = new Client(client1.getId(), "Maria", client1.getLastName(), client1.getEmail(), client1.getAddress());
        assert client2 != null;
        Client updatedClient2 = new Client(client2.getId(), "Ioan", client2.getLastName(), client2.getEmail(), client2.getAddress());

        ClientDAO.update(updatedClient1);
        ClientDAO.update(updatedClient2);

        clients = ClientDAO.getAll();
        System.out.println("Number of current clients: " + clients.size());
        ClientDAO.delete(client1.getId());
        ClientDAO.delete(client2.getId());

        clients = ClientDAO.getAll();
        System.out.println("Number of current clients: " + clients.size());

        String firstName = "Steven";
        String lastName = "King";

        List<Client> clientsByName = ClientDAO.getAllBySelection(firstName, lastName, null);
        List<Client> clientsByNameAndState = ClientDAO.getAllBySelection(firstName, lastName, State.PAID);

        List<Client> clientsByFirstName = ClientDAO.getAllBySelection(firstName, null, null);
        List<Client> clientsByFirstNameAndState = ClientDAO.getAllBySelection(firstName, null, State.PAID);

        List<Client> clientsByLastName = ClientDAO.getAllBySelection(null, lastName, null);
        List<Client> clientsByLastNameAndState = ClientDAO.getAllBySelection(null, lastName, State.ACTIVE);

        System.out.println("Clients with first name " + firstName + " and last name " + lastName);
        clientsByName.forEach(System.out::println);
        System.out.println("Clients with first name " + firstName + " and last name " + lastName + "and reservations with state: " + State.PAID);
        clientsByNameAndState.forEach(System.out::println);

        System.out.println("Clients with first name " + firstName);
        clientsByFirstName.forEach(System.out::println);
        System.out.println("Clients with first name " + firstName + "and reservations with state: " + State.PAID);
        clientsByFirstNameAndState.forEach(System.out::println);

        System.out.println("Clients with last name " + lastName);
        clientsByLastName.forEach(System.out::println);
        System.out.println("Clients with last name " + lastName + "and reservations with state: " + State.ACTIVE);
        clientsByLastNameAndState.forEach(System.out::println);


        /**
         * Some manual test to get / insert / update / delete reservations from / in DB:
         */
        java.sql.Date startDate = java.sql.Date.valueOf("2022-08-01");
        java.sql.Date endDate = java.sql.Date.valueOf("2022-08-31");

        List<Reservation> reservationsBetween = ReservationDAO.getReservationsSelected(String.valueOf(startDate), String.valueOf(endDate), null, null);
        reservationsBetween.forEach(System.out::println);
        System.out.println("number of reservations between " + startDate + " and " + endDate + ": " + reservationsBetween.size());

        Date startDate1 = java.sql.Date.valueOf("2022-04-01");
        Date endDate1 = java.sql.Date.valueOf("2022-08-31");
        List<Reservation> activeBetween = ReservationDAO.getReservationsSelected(String.valueOf(startDate1), String.valueOf(endDate1), State.ACTIVE, null);
        System.out.println("number of active reservations between " + startDate + " and " + endDate + ": " + activeBetween.size());

        Types types = Types.SINGLE;
        String type = types.name();
        List<Reservation> reservationsByType = ReservationDAO.getReservationsSelected(null, null, null, 1L);
        System.out.println("number of reservation for type rooms " + type + ": " + reservationsByType.size());

        List<Reservation> betweenByType = ReservationDAO.getReservationsSelected(String.valueOf(startDate), String.valueOf(endDate), null, 1L);
        System.out.println("number of reservations for type rooms " + type + " between: " + startDate + " and " + endDate + ": " + betweenByType.size());

        Types types1 = Types.DOUBLE;
        String type1 = types1.name();
        List<Reservation> activeBetweenByType = ReservationDAO.getReservationsSelected(String.valueOf(startDate1), String.valueOf(endDate1), State.ACTIVE, 2L);
        System.out.println("number of active reservations for type rooms " + type1 + " between: " + startDate1 + " and " + endDate1 + ": " + activeBetweenByType.size());

        System.out.println("Reservation for client with id 70: " + ReservationDAO.getReservationsByClient(20L, null, null, null));
        System.out.println("Reservation with id: " + ReservationDAO.getById(80));

        List<Reservation> reservationClient = ReservationDAO.getReservationsByClient(null, "Adam", "Fripp", null);
        System.out.println("All reservations for client");
        reservationClient.forEach(System.out::println);

        List<Reservation> activeReservationClient = ReservationDAO.getReservationsByClient(null, "Adam", "Fripp", State.ACTIVE);
        System.out.println("Active reservations for client");
        activeReservationClient.forEach(System.out::println);

        List<Reservation> canceledReservations = ReservationDAO.getReservationsSelected(String.valueOf(startDate1), String.valueOf(endDate1), State.CANCELED, null);
        System.out.println("Canceled reservations between: " + startDate1 + " ~ " + endDate1);
        canceledReservations.forEach(System.out::println);

        List<Reservation> paidReservations = ReservationDAO.getReservationsSelected(String.valueOf(startDate1), String.valueOf(endDate1), State.PAID, null);
        System.out.println("Paid reservations between: " + startDate1 + " ~ " + endDate1);
        paidReservations.forEach(System.out::println);

        //INSERT RESERVATION
        List<Reservation> allReservations = ReservationDAO.getAll();
        System.out.println("number of total reservations registered: " + allReservations.size());

        Reservation r1 = new Reservation(11, java.sql.Date.valueOf("2022-10-01"), java.sql.Date.valueOf("2022-10-05"), 214, null, State.ACTIVE);
        Reservation r2 = new Reservation(12, java.sql.Date.valueOf("2022-10-06"), java.sql.Date.valueOf("2022-10-20"), 314, null, State.ACTIVE);

        ReservationDAO.insert(r1);
        ReservationDAO.insert(r2);

        List<Reservation> allReservationsAfter = ReservationDAO.getAll();
        System.out.println("number of total reservations registered after insert: " + allReservationsAfter.size());

        Reservation r1Inserted = allReservationsAfter.stream()
                .filter(i -> (i.getClientId() == r1.getClientId() &&
                        i.getStartDate().toString().equalsIgnoreCase(r1.getStartDate().toString()) &&
                        i.getState().equals(r1.getState()) &&
                        i.getEndDate().toString().equalsIgnoreCase(r1.getEndDate().toString())))
                .findFirst()
                .orElse(null);

        Reservation r2Inserted = allReservationsAfter.stream()
                .filter(i -> (i.getClientId() == r2.getClientId() &&
                        i.getStartDate().toString().equalsIgnoreCase(r2.getStartDate().toString()) &&
                        i.getState().equals(r2.getState()) &&
                        i.getEndDate().toString().equalsIgnoreCase(r2.getEndDate().toString())))
                .findFirst()
                .orElse(null);

        assert r1Inserted != null;
        System.out.println("Reservation 1 id (inserted): " + r1Inserted.getId() + " " + r1Inserted);
        assert r2Inserted != null;
        System.out.println("Reservation 2 id (inserted): " + r2Inserted.getId() + " " + r2Inserted);

        //METHOD DAYS BETWEEN
        System.out.println("days between r1 " + Invoice.getDaysBetween(r1Inserted.getStartDate(), r1Inserted.getEndDate()));
        System.out.println("days between r2 " + Invoice.getDaysBetween(r2Inserted.getStartDate(), r2Inserted.getEndDate()));

        //UPDATE RESERVATION
        Reservation updateR1 = new Reservation(r1Inserted.getId(), r1Inserted.getStartDate(), java.sql.Date.valueOf("2022-10-06"), r1Inserted.getRoomId(), "end date updated 1", State.CANCELED);
        Reservation updateR2 = new Reservation(r2Inserted.getId(), r2Inserted.getStartDate(), java.sql.Date.valueOf("2022-10-21"), r2Inserted.getRoomId(), "end date updated 2", State.PAID);

        List<Reservation> allReservationTestAfter = ReservationDAO.getAll();
        System.out.println("Size after inserting 2 reservations: " + allReservationTestAfter.size());

        ReservationDAO.update(updateR1);
        ReservationDAO.update(updateR2);

        List<Reservation> allReservationUpdate = ReservationDAO.getAll();

        Reservation res1 = allReservationUpdate.stream()
                .filter(i -> i.getId() == r1Inserted.getId())
                .findFirst()
                .orElse(null);

        Reservation res2 = allReservationUpdate.stream()
                .filter(i -> i.getId() == r2Inserted.getId())
                .findFirst()
                .orElse(null);

        System.out.println("updated reservation 1: " + res1);
        System.out.println("updated reservation 2: " + res2);

        //DELETE RESERVATION
        ReservationDAO.delete(r1Inserted.getId());
        ReservationDAO.delete(r2Inserted.getId());

        List<Reservation> allReservationFinal = ReservationDAO.getAll();

        System.out.println("number of registered reservations after delete: " + allReservationFinal.size());


        /**
         * Some manual test to get Rooms, RoomsFull and Room Types from DB
         */
        long idCheck = 104;
        List<Room> allRooms = RoomDAO.getAll();
        allRooms.forEach(System.out::println);
        System.out.println("Number of rooms in hotel: " + allRooms.size());

        Room room = RoomDAO.getById(idCheck).orElse(null);
        System.out.println("room with id " + idCheck + ": " + room);
        System.out.println("has sea view: " + Invoice.hasSeaView(idCheck));

        assert room != null;
        long typeId = room.getRoomTypeId();

        List<RoomTypes> allRoomTypes = RoomTypesDAO.getAll();
        allRoomTypes.forEach(System.out::println);
        System.out.println("Number of room types in hotel: " + allRoomTypes.size());

        RoomTypes type7 = RoomTypesDAO.getById(typeId).orElse(null);
        System.out.println("type of room with id " + idCheck + ": " + type7);
        assert type7 != null;
        System.out.println("rate of room with id " + idCheck + ": " + type7.getRate());
        System.out.println("capacity of room with id " + idCheck + ": " + type7.getCapacity());

        // test get AVAILABLE ROOMS from DB between DATE and TYPE
        String startDate5 = "2022-08-01";
        String endDate5 = "2022-08-31";

        Types types8 = Types.SINGLE;
        String type8 = types8.name();

        Types types9 = Types.TWIN;
        String type9 = types9.name();

        Types types10 = Types.DOUBLE;
        String type10 = types10.name();

        Types types11 = Types.TRIPLE;
        String type11 = types11.name();

        Types types12 = Types.SUITE;
        String type12 = types12.name();

        List<Room> room8 = RoomDAO.getAvailableFromSelection(startDate5, endDate5, 1L);
        List<Room> room9 = RoomDAO.getAvailableFromSelection(startDate5, endDate5, 3L);
        List<Room> room10 = RoomDAO.getAvailableFromSelection(startDate5, endDate5, 2L);
        List<Room> room11 = RoomDAO.getAvailableFromSelection(startDate5, endDate5, 4L);
        List<Room> room12 = RoomDAO.getAvailableFromSelection(startDate5, endDate5, 5L);

        List<Room> allRooms1 = RoomDAO.getAvailableFromSelection(startDate5, endDate5, null);

        System.out.println("Available rooms list between " + startDate5 + " - " + endDate5
                + "of type: " + type8 + "\n" + room8);

        System.out.println("Available rooms list between " + startDate5 + " - " + endDate5
                + "of type: " + type9 + "\n" + room9);

        System.out.println("Available rooms list between " + startDate5 + " - " + endDate5
                + "of type: " + type10 + "\n" + room10);

        System.out.println("Available rooms list between " + startDate5 + " - " + endDate5
                + "of type: " + type11 + "\n" + room11);

        System.out.println("Available rooms list between " + startDate5 + " - " + endDate5
                + "of type: " + type12 + "\n" + room12);

        System.out.println("All available rooms list between " + startDate5 + " - "
                + endDate5 + "\n");
        allRooms1.forEach(System.out::println);

        List<RoomFull> allRoomsFull = RoomDAO.getAllFull();
        allRoomsFull.forEach(System.out::println);
        boolean checkSize = allRoomsFull.size() == allRooms.size();
        System.out.println("Number of rooms in hotel: " + allRoomsFull.size() +
                " number of total rooms = number of total rooms with full details: " + checkSize);

        List<RoomFull> allRoomsFullAvailable = RoomDAO.getFullAvailableFromSelection(startDate5, endDate5, null);
        allRoomsFullAvailable.forEach(System.out::println);
        boolean checkSizeAvailable = allRoomsFullAvailable.size() == allRooms1.size();
        System.out.println("Available rooms with full details list between " + startDate5 + " - " + endDate5
                + " is equal with number of rooms available list: " + checkSizeAvailable);

        List<RoomFull> allRoomsFullAvailableType = RoomDAO.getFullAvailableFromSelection(startDate5, endDate5, 2L);
        allRoomsFullAvailableType.forEach(System.out::println);
        boolean checkSizeAvailableType = allRoomsFullAvailableType.size() == room10.size();
        System.out.println("Available rooms with full details list between " + startDate5 + " - " + endDate5
                + " of type " + type10 + " is equal with number of rooms available list: " + checkSizeAvailableType);


        /**
         * Some manual test to check methods for Seasons, dates, Invoice
         */
        List<Reservation> allReservations8 = ReservationDAO.getAll();

        Reservation res8 = allReservations8.stream()
                .filter(i -> i.getId() == 47) //startDate: 2022-06-17, endDate: 2022-06-30
                .findFirst()
                .orElse(null);

        Reservation res9 = allReservations8.stream()
                .filter(i -> i.getId() == 85)   //startDate: 2022-09-01, endDate: 2022-09-30
                .findFirst()
                .orElse(null);

        Reservation res10 = allReservations8.stream()
                .filter(i -> i.getId() == 88)   //startDate: 2022-10-14, endDate: 2022-10-30
                .findFirst()
                .orElse(null);

        Reservation res11 = allReservations8.stream()
                .filter(i -> i.getId() == 99)   //startDate: 2022-12-12, endDate: 2022-12-18
                .findFirst()
                .orElse(null);

        System.out.println("reservations loaded from DB: " + "\n" +
                res8 + "\n" + res9 + "\n" + res10 + "\n" + res11 + "\n");

        assert res8 != null;
        RoomTypes type88 = Invoice.getRoomType(res8.getRoomId());
        assert res9 != null;
        RoomTypes type99 = Invoice.getRoomType(res9.getRoomId());
        assert res10 != null;
        RoomTypes type110 = Invoice.getRoomType(res10.getRoomId());
        assert res11 != null;
        RoomTypes type111 = Invoice.getRoomType(res11.getRoomId());

        System.out.println("reservations type of rooms: " + "\n" +
                type88 + "\n" + type99 + "\n" + type110 + "\n" + type111 + "\n");

        Room room15 = RoomDAO.getById(res8.getRoomId()).orElse(null);
        Room room25 = RoomDAO.getById(res9.getRoomId()).orElse(null);
        Room room35 = RoomDAO.getById(res10.getRoomId()).orElse(null);
        Room room45 = RoomDAO.getById(res11.getRoomId()).orElse(null);

        System.out.println("rooms reserved details: " + "\n" +
                room15 + "\n" + room25 + "\n" + room35 + "\n" + room45 + "\n");

        System.out.println("rooms reserved have sea view: " + "\n" +
                "room 1: " + Invoice.hasSeaView(res8.getRoomId()) + "\n" +
                "room 2: " + Invoice.hasSeaView(res9.getRoomId()) + "\n" +
                "room 3: " + Invoice.hasSeaView(res10.getRoomId()) + "\n" +
                "room 4: " + Invoice.hasSeaView(res11.getRoomId()) + "\n");

        Date s1 = res8.getStartDate();
        Date e1 = res8.getEndDate();
        Date s2 = res9.getStartDate();
        Date e2 = res9.getEndDate();
        Date s3 = res10.getStartDate();
        Date e3 = res10.getEndDate();
        Date s4 = res11.getStartDate();
        Date e4 = res11.getEndDate();

        System.out.println("number of days for reservations selected: " + "\n" +
                "reservation 1: " + Invoice.getDaysBetween(s1, e1) + "\n" +
                "reservation 2: " + Invoice.getDaysBetween(s2, e2) + "\n" +
                "reservation 3: " + Invoice.getDaysBetween(s3, e3) + "\n" +
                "reservation 4: " + Invoice.getDaysBetween(s4, e4) + "\n");

        System.out.println("number of days in Summer Season & Cold Season for reservations selected" + "\n" +
                "reservation 1: summer " + Invoice.getDaysSummerSeason(s1, e1) + ", cold " + Invoice.getDaysColdSeason(s1, e1) + "\n" +
                "reservation 2: summer " + Invoice.getDaysSummerSeason(s2, e2) + ", cold " + Invoice.getDaysColdSeason(s2, e2) + "\n" +
                "reservation 3: summer " + Invoice.getDaysSummerSeason(s3, e3) + ", cold " + Invoice.getDaysColdSeason(s3, e3) + "\n" +
                "reservation 4: summer " + Invoice.getDaysSummerSeason(s4, e4) + ", cold " + Invoice.getDaysColdSeason(s4, e4) + "\n");

        long id1 = res8.getId();
        long id2 = res9.getId();
        long id3 = res10.getId();
        long id4 = res11.getId();

        System.out.println("clients id: " + res8.getClientId() + " " + res9.getClientId() +
                " " + res10.getClientId() + " " + res11.getClientId());

        System.out.println("invoice for reservations selected: " + "\n" +
                "reservation 1: " + "\n" + Invoice.getInvoiceById(id1) + "\n" +
                "reservation 2: " + "\n" + Invoice.getInvoiceById(id2) + "\n" +
                "reservation 3: " + "\n" + Invoice.getInvoiceById(id3) + "\n" +
                "reservation 4: " + "\n" + Invoice.getInvoiceById(id4) + "\n");


        List<RoomFull> allRoomsFull1 = RoomDAO.getAllFull();
        allRoomsFull1.forEach(System.out::println);
        Date d1 = Date.valueOf("2022-08-01");
        Date d2 = Date.valueOf("2022-08-31");


        List<ReservationFull> allReservations21 = ReservationDAO.getFullReservationsSelected(null, null);
        allReservations21.forEach(System.out::println);

        List<ReservationFull> all = ReservationDAO.getAllFull();
        all.forEach(System.out::println);
        String invoice51 = Invoice.getInvoiceById(51);

        System.out.println(invoice51);
        Reservation reservation21 = new Reservation(1, Date.valueOf("2022-01-07"), Date.valueOf("2022-01-11"), 101, "update", State.PAID);
        ReservationDAO.update(reservation21);

        Date d3 = Date.valueOf("2022-08-01");
        System.out.println(Invoice.getDaysBetween(d1, d3));

        System.out.println(Invoice.getDaysBetween(d3, d1));

        String from = "2022-08-01";
        String until = "2022-08-31";
        List<RoomFull> allRooms31 = RoomDAO.getFullAvailableFromSelection(null, until, 2L);
        allRooms31.forEach(System.out::println);
        System.out.println(allRooms31.size());

        List<RoomFull> all31 = RoomDAO.getAllFull();
        all31.forEach(System.out::println);

        List<RoomFull> allRooms41 = RoomDAO.getFullAvailableFromSelection(from, until, null);
        long singleCount = RoomDAO.getAvailableFromSelection(null, null, 1L).size();
        long doubleCount = RoomDAO.getAvailableFromSelection(null, null, 2L).size();
        long twinCount = RoomDAO.getAvailableFromSelection(null, null, 3L).size();
        long tripleCount = RoomDAO.getAvailableFromSelection(null, null, 4L).size();
        long suiteCount = RoomDAO.getAvailableFromSelection(null, null, 5L).size();

        long availableCount = allRooms41.size();
        long reservedCount = RoomDAO.getAll().size() - availableCount;

        System.out.println("single: " + singleCount + " double: " + doubleCount + " twin: " +
                twinCount + " triple: " + tripleCount + " suite: " + suiteCount);

        System.out.println("available: " + availableCount + " reserved: " + reservedCount);

        List<RoomTypes> allTypes = RoomTypesDAO.getAll();
        allTypes.forEach(System.out::println);
    }
}
