## Wantsome - Hotel Reservation System

This is an application that facilitates the tracking and administration process of hotel reservations.

---

### 1. Description

It is a Hotel Reservations System that allows hotel employees (from the reception for example) to have a record of hotel
guests, and for each of them, to add reservations, by selecting an available room during the period when the reservation
will be registered.

__The application revolves around three main components:__

- Clients (guests)
- Rooms
- Reservations

For each of these components allows CRUD operations (create, read, update and delete). What is important to mention here
is that these operations can be performed based on some logical restrictions, as follows:

__A. Clients__

- __Create__: a new Client can be registered only if first and last name introduced are alphabetical, the address has at
  least 3 characters and the email address is properly formatted, but also if it's UNIQUE (meaning we cannot have two
  different clients with same email address)
  ![add_client](doc/add_client.png)
- __Read__: the list of all Clients registered so far is displayed with some sorting and searching options such as:
  - sorting list of Clients by first or last name
  - searching for a specific Client by e-amil address (unique property for every Client registered)
- __Update__: all fields (first and last name, e-mail, address) of an existing Client can be updated only if all
  conditions from _Create_ operation are fulfilled
  ![update_client](doc/update_client.png)
- __Delete__: this operation can pe performed only on Clients that have no reservations registered. If we want to delete
  a Client from application, we first have to delete all reservations related
  ![delete_client](doc/delete_client.png)
  __B. Rooms__
- __Create__: a new Room can be registered only if the room number introduced is UNIQUE (
  meaning we cannot have two different rooms with same number) and the details about room include at least 10
  characters (details such as: floor and view)
  ![add_room](doc/add_room.png)
- __Read__: the list of all Rooms registered so far is displayed with some sorting, filtering and searching options such
  as:
  - filtering list of Rooms by type of room _(All, Single, Double, Twin, Triple, Suite)_
  - sorting list of Rooms by rate _(in RON)_ or capacity _(number of persons)_
  - searching for available Rooms in a certain period _(by selecting date: from ~ until)_
- __Update__: for an existing Room we can modify only two fields: type of room and details. The number of room cannot be
  modified
  ![update_room](doc/update_room.png)
- __Delete__: this operation can be performed only on Rooms that have no related reservations registered. If we want to
  delete a Room from application, we first have to delete all reservations related
  ![delete_room](doc/delete_room.png)
  __C. Reservations__
- __Create__: a new Reservation can be registered only from Clients interface, and only for a selected Client. The
  number of room for which a reservation is made can be selected from a list with all available rooms in specified
  period. Create operation is performed when the following fields are selected correctly:
  - start date of reservation: start date cannot be older than the current date
  - end date of reservation: end date cannot be older than the start date
    ![add_reservation](doc/add_reservation.png)
- __Read__: the list of all Reservations registered so far is displayed with some sorting, filtering and searching
  options such as:
  - filtering list of Reservations by state _(All, Active, Paid, Canceled)_
  - displaying list of Reservations for a certain __Client__ (from Clients page)
    ![display_reservations_client](doc/display_reservations_client.png)
  - sorting list of Reservations by start date or end date
  - searching for Reservations in a certain period _(by selecting date: start date ~ end date)_
    ![display_reservations](doc/display_reservations.png)
- __Update__: this operation is performed based on the state of an existing Reservation as follows:
  - state Active:
    - when reservation refers to a future period: we can update all fields
    - when reservation is in progress: we can update all fields EXCEPT start date (already started)
      and room number
  - state Paid: we can change only the state of it, form Paid in Canceled and details field
  - state Canceled: no update action for canceled Reservations
    ![update_reservation](doc/update_reservation.png)
- __Delete__: this operation can be performed only on active Reservations

Based on this main components, the application computes an invoice for each reservation, no matter its state, and some
reports.

__D. Invoice__

- it's computed for all reservations registered
- we have the option to print it (printable area)
- contains details about Client, period and room reserved and also about charged rate, determined according to the
  season (Summer Season, Cold Season, no season) and the view of the room:
  ![invoice](doc/invoice.png)
- here is the Price Policy used currently in application:
  ![price_policy](doc/price_policy.png)

__E. Reports__

1. Report reflecting hotel __occupancy rate__ in a certain period:

- we can select the period for which this rate is computed
- we will see a _pie-chart_ for available and reserved rooms
  ![report 1](doc/report1.png)

2. Report reflecting __state of reservations__ in a certain period:

- we can select the period for which this rate is computed
- we will see a _pie-chart_ for active and paid reservations
  ![report 2](doc/report2.png)

3. Report reflecting __structure of all rooms__ by their type (Single, Double, Twin, Triple or Suite)
   ![report 3](doc/report3.png)

---

### 2. Setup

No setup needed, just start the application. If the database is missing (like on first startup), it will create a new
database (of type SQLite, stored in a local file named 'Hotel_Reservation.db'), and use it to save the future data.

Once started, access it with a web browser at:
http://localhost:8080

---

### 3. Technical details

__User interface__

The application have a web app for user interface that starts with Main class

__Technologies__

- main code is written in Java (version 11)
- it uses [SQLite](https://www.sqlite.org/), a small embedded database, for its persistence, using SQL and JDBC to
  access it from Java code
- it uses [Javalin](https://javalin.io/) micro web framework (which includes an embedded web server, Jetty)
- it uses [Velocity](https://velocity.apache.org/) templating engine, to separate the UI code from Java code; UI code
  consists of basic HTML and CSS code (and some Javascript)
- charts: using JavaScript library Google Charts

---

### 4. Future plans

Other features which could be added in the future:

- possibility to reserve more than one room through a single reservation
- make it multi-user with different levels of access like administrator and receptionist; user must log in at start
- unit tests for DB part (using [JUnit](https://junit.org/junit4/))
